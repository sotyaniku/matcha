<?php

require_once ROOT . '/model/User.php';
require_once ROOT . '/model/Session.php';

class Status {
    private $user = '';
    private $s_id = '';

    public function __construct() {
        if (!empty($_SESSION['logged_user']))
            $this->user = $_SESSION['logged_user'];
    }

    public function update() {
        date_default_timezone_set('Europe/Kiev');

        $time = date("Y-m-d H:i:s");

        //$checktime = new DateTime();
        //$checktime->modify('-5 minutes');
        //
        //Session::deleteOldSessions($checktime->format('Y-m-d H:i:s'));

        if ($this->user)
        {
            $user_info = User::getUserByLogin($this->user);
            $this->s_id = session_id();

            if (Session::getSession($user_info['user_id'])) {
                Session::updateSession($user_info['user_id'], $this->s_id, $time);
            }
            else {
                Session::setSession($user_info['user_id'], $this->s_id, $time);
            }
        }
    }


}