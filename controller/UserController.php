<?php

require_once ROOT . '/model/User.php';
require_once ROOT . '/model/Photo.php';

class UserController extends Controller
{
    private $user;
    private $username;
    private $firstname;
    private $lastname;
    private $gender;
    private $password;
    private $email;
    private $preferences;
    private $latitude;
    private $longitude;
    private $country;

    private $from = 'Matcha bot';
    private $reset_msg = 'Please follow the link to update your password: '
    . SITE_HOST . SITE_BASE_URI . 'resetpass/';


    public function __construct()
    {
        parent::__construct();
        if (!empty($_SESSION['logged_user']))
            $this->user = User::getUserAllInfoByLogin($_SESSION['logged_user']);
    }

    public function actionIndex()
    {
        if (empty($_SESSION['allowed'])) {
            $this->view->generate('AgeConfirmView.php', 'CleanTemplateView.php');
            return;
        }
        if ($this->user) {
            $this->view->generate('MainView.php', 'TemplateView.php', $this->user);
            return;
        }
        $this->view->generate('LoginView.php', 'CleanTemplateView.php');
    }

    public function actionStatus()
    {
        $user = User::getUserByLogin($_POST['login']);
        $session = Session::getSession($user['user_id']);

        date_default_timezone_set('Europe/Kiev');
        $checktime = new DateTime();

        $sessiontime = DateTime::createFromFormat('Y-m-d H:i:s', $session["last_time"]);
        $interval = $checktime->diff($sessiontime);

        if ($interval->format('%i') < 5)
            echo "OK";

        else {
            if ($interval->format('%d') >= 1)
                echo $sessiontime->format('Y-m-d H:i:s');
            else if ($checktime->format('%h') < $sessiontime->format('%h'))
                echo "yesterday at " . $sessiontime->format('H:i:s');
            else
                echo "today at " . $sessiontime->format('H:i:s');
        }
    }

    public function actionNotification()
    {
        $notifications = User::countNotifications($_POST['id'], 0);
        echo $notifications[0];
    }

    public function actionShownotifications()
    {
        $notifications = User::showNotifications($this->user[0]['user_id'], 0);
        $this->view->generate('NotificationView.php', 'TemplateView.php', $this->user, $notifications);
    }

    public function actionEncounters($sort_type = null)
    {
        $interests = $this->_getInterestsArray($this->user);
        $sort_param = "distance, common_interests DESC, user_rating DESC ";
        $preferences = $this->_setPreferencesConditions($this->user[0]['user_preferences']);

        if (!empty($_POST['sort'])) {
            $filter_params['tags'] = isset($_POST['tags']) ? $_POST['tags'] : 0;
            $filter_params['rating'] = isset($_POST['rating']) ? $_POST['rating'] : 0;
            $filter_params['distance'] = isset($_POST['distance']) ? $_POST['distance'] : 40;

            $matches = User::showFilteredSuggestions($this->user[0], $interests, $sort_param, $filter_params, $preferences);
            $this->view->generate('EncountersView.php', 'TemplateView.php', $this->user, $matches);
            return;
        }

        if (preg_match('/^order-(distance|rating|tags)$/', $sort_type))
            $sort_param = $this->_setSortParams($sort_type);

        $matches = User::showSuggestions($this->user[0], $interests, $sort_param, $preferences);

        $this->view->generate('EncountersView.php', 'TemplateView.php', $this->user, $matches);
    }

    public function actionSearch()
    {
        $interests = $this->_getInterestsArray($this->user);
        $sort_param = "distance, user_rating DESC ";
        $search_param = array();
        $search_interests = array();
        $preferences = $this->_setPreferencesConditions($this->user[0]['user_preferences']);

        $int_list = User::InterestList();
        $this->user['int_list'] = $int_list;

        $filter_params['tags'] = isset($_POST['tags']) ? intval($_POST['tags']) : '0';
        $filter_params['rating'] = isset($_POST['rating']) ? intval($_POST['rating']) : '0';
        $filter_params['distance'] = isset($_POST['distance']) ? intval($_POST['distance']) : '2500';

        if (!empty($_POST['order']) && preg_match('/^order-(distance|rating|tags)$/', $_POST['order'])) {
            $sort_param = $this->_setSortParams($_POST['order']);
        }

        $search_param['rating_from'] = !empty($_POST['rating-from']) ? floatval($_POST['rating-from']) : '0';
        $search_param['rating_to'] = !empty($_POST['rating-to']) ? floatval($_POST['rating-to']) : '5';
        $search_param['age_from'] = !empty($_POST['age-from']) ? intval($_POST['age-from']) : '18';
        $search_param['age_to'] = !empty($_POST['age-to']) ? intval($_POST['age-to']) : '100';
        $search_param['distance_from'] =
            !empty($_POST['distance-from']) ? intval($_POST['distance-from']) : '0';
        $search_param['distance_to'] =
            !empty($_POST['distance-to']) ? intval($_POST['distance-to']) : '25000';

        if (!empty($_POST['interest']) && $_POST['interest'][0] != 'any') {
            foreach ($_POST['interest'] as $row) {
                $search_interests[] = "'" . $row . "'";
                $search_param['interest'][] = $row;
            }
            $search_interests = implode(',', $search_interests);
        } else {
            $search_interests = "''";
        }

        $int_list = $search_interests == "''" ? "" : "WHERE interest_name IN (" . $search_interests . ")";

        if (empty($_POST['age-to']) && empty($_POST['age-from'])) {
            $matches = User::searchAllAgesPeople($this->user[0], $sort_param, $preferences, $int_list, $search_param, $filter_params);
            $search_param['age_from'] = '';
            $search_param['age_to'] = '';
        }
        else
            $matches = User::searchPeople($this->user[0], $sort_param, $preferences, $int_list, $search_param, $filter_params);

        $this->user['search'] = $search_param;

        $this->view->generate('SearchView.php', 'TemplateView.php', $this->user, $matches);
        return;
    }

    public function actionUserpage($login)
    {
        if (isset($_GET) && $_GET) {
            list($login) = explode('?', $login);
            if (isset($_GET['visit']))
                User::updateSeenVisits($this->user[0]['user_id'], 1);
            else
                User::updateSeenLikes($this->user[0]['user_id'], 1);
        }

        $profile = User::getUserByLogin($login);
        $time = date("Y-m-d H:i:s");

        if ($profile == NULL)
            header('Location: /matcha/');
        if ($profile['user_id'] != $this->user[0]['user_id']) {
            if (!($visit = User::findVisit($this->user[0]['user_id'], $profile['user_id'])))
                User::addVisit($this->user[0]['user_id'], $profile['user_id'], $time);
            else
                User::changeVisit($this->user[0]['user_id'], $profile['user_id'], 0, $time);

            $like_back = User::findLike($profile['user_id'], $this->user[0]['user_id']);
        }

        if (!empty($_POST['photo-src'])) {
            if ($profile) {
                $url = $this->_saveImage($profile['user_id'], $_POST['photo-src']);
                User::addPhoto($profile['user_id'], $url);
                header("Location: /matcha/$login");
            }
        } else if (!empty($_POST['delete-photo-id'])) {
            $photo = Photo::getPhotoById($_POST['delete-photo-id']);
            $this->_deleteImage($photo['photo_src']);
            Photo::removePhoto($_POST['delete-photo-id']);
        } else if (isset($_POST['like'])) {
            if (NULL == $like = User::findLike($this->user[0]['user_id'], $profile['user_id'])) {
                User::addLike($this->user[0]['user_id'], $profile['user_id'], $time);
                if (User::getRating($profile['user_id']) <= 4.95)
                    User::updateRating($profile['user_id'], "0.05");
            } else {
                switch ($_POST['like']) {
                    case 0:
                        User::changeLike($this->user[0]['user_id'], $profile['user_id'], 1, 0, $time);
                        if (User::getRating($profile['user_id']) <= 4.95)
                            User::updateRating($profile['user_id'], "0.05");
                        break;
                    case 1:
                        User::changeLike($this->user[0]['user_id'], $profile['user_id'], 0, 0, $time);
                        if (User::getRating($profile['user_id']) >= 1.05)
                            User::updateRating($profile['user_id'], "-0.05");
                        break;
                };
            }
        }

        if (!empty($_POST['block']) && $profile['user_id'] != $this->user[0]['user_id']) {
            if (!User::checkIfIsBlocked($this->user[0]['user_id'], $profile['user_id'])) {
                User::blockUser($this->user[0]['user_id'], $profile['user_id']);
                if (User::getRating($profile['user_id']) >= 1.2)
                    User::updateRating($profile['user_id'], "-0.2");
                header("Location: /matcha/$login");
            }
        } else if (!empty($_POST['fake']) && $profile['user_id'] != $this->user[0]['user_id']) {
            User::reportFakeUser($profile['user_id']);
            header("Location: /matcha/$login");

        }

        $user_info = User::getUserAllInfoByLogin($login);
        if ($photos = User::getUserPhotosByLogin($login))
            $user = array($photos, $user_info);
        else
            $user = array($user_info);

        $like = User::findLike($this->user[0]['user_id'], $profile['user_id']);
        $user[2] = !empty($like['like_status']) ? $like['like_status'] : 0;
        $user[3] = !empty($like_back['like_status']) ? $user[2] && $like_back['like_status'] : 0;

        $this->view->generate('UserpageView.php', 'TemplateView.php', $this->user, $user);
    }

    public function actionHistory()
    {

        $visits = User::showHistory($this->user[0]['user_id']);
        $this->view->generate('HistoryView.php', 'TemplateView.php', $this->user, $visits);
    }

    public function actionVisitors()
    {

        $visits = User::showVisits($this->user[0]['user_id']);
        $this->view->generate('VisitsView.php', 'TemplateView.php', $this->user, $visits);
    }

    public function actionLikes()
    {

        $likes = User::showLikes($this->user[0]['user_id']);
        $this->view->generate('LikesView.php', 'TemplateView.php', $this->user, $likes);
    }

    public function actionConfirm()
    {
        $_SESSION['allowed'] = 'allowed';
        header('Location: /matcha/');
    }

    public function actionReject()
    {
        $_SESSION['allowed'] = '';
        header('Location: /matcha/');
    }

    public function actionLogout()
    {
        $_SESSION['logged_user'] = '';
        session_destroy();
        header('Location: /matcha/');
    }

    public function actionUpdateprofile()
    {
        if (!$this->user)
            header('Location: /matcha/');

        if (!empty($_POST['update-profile'])) {
            $user = User::getUserByLogin($_POST['username']);

            if ($user['user_mail'] != $_POST['email'] && $this->_checkEmailInDb($_POST['email'])) {
                echo "Email";
                return;
            }

            if (!empty($_POST['password'])) {
                $this->password = hash('whirlpool', $_POST['password']);
                User::updateUserPassword($_POST['user_id'], $this->password);
            }

            if (!empty($_POST['photo-src'])) {
                $user = User::getUserById($_POST['user_id']);
                $this->_deleteImage($user['user_profilephoto']);
                $photo_src = $this->_saveImage($_POST['user_id'], $_POST['photo-src']);
                User::updateUserPhoto($_POST['user_id'], $photo_src);
            }
            $biography = htmlentities($_POST['biography'], ENT_QUOTES);
            $new_interests = !empty($_POST['interest']) ? $_POST['interest'] : array();
            $interests = User::getInterestsById($_POST['user_id']);
            $this->_updateInterests($interests, $new_interests);

            User::updateUserInfo($_POST['user_id'], $_POST['firstname'], $_POST['lastname'], $_POST['email'],
                $_POST['gender'], $_POST['preferences'], $_POST['age'], $_POST['latitude'], $_POST['longitude'],
                $biography, $_POST['country']);
        }
        echo "OK";
    }

    public function actionProfile()
    {
        if (!$this->user)
            header('Location: /matcha/');

        $user = User::getUserAllInfoByLogin($_SESSION['logged_user']);
        $interests = User::getInterestsList($_SESSION['logged_user']);
        $this->view->generate("ProfileView.php", "TemplateView.php", $user, $interests);
    }

    public function actionResetpass($id = null, $url = null)
    {
        $data = array();
        $data['msg'] = "Please check your email for a password reset link.";
        $data['header'] = "Forgot your password?";
        $data['id'] = $id;
        if ($url) {
            if ($this->_createPasswordUrl($id) == $url) {
                $this->view->generate("UpdatepassView.php", "CleanTemplateView.php", $data);
                return;
            }
            $data['msg'] = "Your password reset link is invalid.";
        }
        $this->view->generate("ResetpassView.php", "CleanTemplateView.php", $data);
        return;
    }

    public function actionUpdatepass()
    {
        $data = array();
        $data['msg'] = "Your password reset link is invalid.";
        $data['header'] = "Update your password";
        if (!empty($_POST['updatepass'])) {
            $id = $_POST['id'];
            $password = hash('whirlpool', $_POST['password']);
            $data['msg'] = "Your password has been successfully updated!";
            User::updateUserPassword($id, $password);
        }
        $this->view->generate("ResetpassView.php", "CleanTemplateView.php", $data);
    }

    public function actionSendpass()
    {
        if (!empty($_POST['sendpass'])) {
            if (!($user = $this->_checkEmailInDb($_POST['email'])))
                echo "Sendpass error";
            else {
                echo "Sendpass success";
                $this->email = $_POST['email'];
                $url = $this->_createPasswordUrl($user['user_id']);
                mail($this->email, "Matcha: reset password",
                    $this->reset_msg . "{$user['user_id']}/" . $url, $this->from);
            }
            return;
        }
        $this->view->generate("SendpassView.php", "CleanTemplateView.php");
    }

    public function actionRegister()
    {
        if (!empty($_POST['register'])) {
            if ($this->_findUserInDb($_POST['username']))
                echo "Username";
            else if ($this->_checkEmailInDb($_POST['email']))
                echo "Email";
            else {
                $this->username = $_POST['username'];
                $this->password = hash('whirlpool', $_POST['password']);
                $this->email = $_POST['email'];
                $this->firstname = $_POST['firstname'];
                $this->lastname = $_POST['lastname'];
                $this->gender = $_POST['gender'];
                $this->preferences = $_POST['preferences'];
                $this->latitude = (!empty($_POST['latitude'])) ? $_POST['latitude'] : 0;
                $this->longitude = (!empty($_POST['longitude'])) ? $_POST['longitude'] : 0;
                $this->country = (!empty($_POST['country'])) ? $_POST['country'] : '';
                if (!$this->latitude && !$this->longitude)
                    $this->_setLocationByIP($this->latitude, $this->longitude, $this->country);
                User::addUserToDb($this->username, $this->firstname, $this->lastname,
                    $this->email, $this->password, $this->gender, $this->preferences, $this->latitude, $this->longitude, $this->country);
                echo "OK";
            }
        } else
            header('Location: /matcha/');
    }

    public function actionLogin()
    {
        if (!empty($_POST['log-in'])) {
            if (!$this->_checkUserAuth($_POST['username'], hash('whirlpool', $_POST['password'])))
                echo "Login";
            else {
                $_SESSION['logged_user'] = $_POST['username'];
                echo "Main";
            }
        } else
            header('Location: /matcha/');
    }

    public function actionMessages($login = null)
    {
        $data = array();
        $data['messages'] = User::getAllConversations($this->user[0]['user_id']);

        if (isset($login)) {
            $data['companion'] = $login;
            $partner = User::getUserByLogin($login);
            $data['chat'] = User::getMessages($this->user[0]['user_id'], $partner['user_id']);
            $data['blocked'] = User::checkIfIsBlocked($this->user[0]['user_id'], $partner['user_id']) ||
                User::checkIfIsBlocked($partner['user_id'], $this->user[0]['user_id']) ||
                !User::getLikeStatus($partner['user_id'], $this->user[0]['user_id']) ||
                !User::getLikeStatus($this->user[0]['user_id'], $partner['user_id']);

            if (isset($_POST['message']) && $_POST['message']) {
                date_default_timezone_set('Europe/Kiev');
                $time = date("Y-m-d H:i:s");
                $message = htmlentities($_POST['message'], ENT_QUOTES);
                User::writeMessage($this->user[0]['user_id'], $partner['user_id'], $time, nl2br($message));
                header("Location: /matcha/messages/$login");

            }
        }
        $this->view->generate('MessagesView.php', 'TemplateView.php', $this->user, $data);
    }

    private function _setLocationByIP(&$lat, &$lon, &$country)
    {
        $info = json_decode(file_get_contents('https://freegeoip.net/json/'));
        $lat = $info->latitude;
        $lon = $info->longitude;
        $country = $info->country_code;
    }

    private function _findUserInDb($login)
    {
        $user = User::getUserByLogin($login);
        return $user;
    }

    private function _checkEmailInDb($email)
    {
        $user = User::getUserByEmail($email);
        return $user;
    }

    private function _checkUserAuth($login, $password)
    {
        if (!($user = User::getUserByLogin($login)))
            return 0;
        return ($user['user_login'] == $login && $user['user_password'] == $password);
    }

    private function _createPasswordUrl($id)
    {
        $salt = '!QAsw2^HKJJnkwe$%F+LMHK';
        $passwordUrl = hash('whirlpool', $id . $salt);
        return $passwordUrl;
    }

    private function _saveImage($id, $src)
    {
        if (!file_exists(ROOT . '/webroot/photos/' . $id))
            mkdir(ROOT . '/webroot/photos/' . $id);
        $format = '';
        $img = $this->_convertImage($src, $format);
        $url = "webroot/photos/" . $id . "/" . uniqid() . "." . $format;
        $img_url = ROOT . "/" . $url;
        file_put_contents($img_url, $img);
        return SITE_BASE_URI . $url;
    }

    private function _convertImage($src, &$format)
    {
        preg_match('@data:image\/(\w+);base64,(.+)@', $src, $matches);
        $src = chunk_split(str_replace(' ', '+', $matches[2]));
        $format = $matches[1];
        $img = base64_decode($src);
        return ($img);
    }

    private function _deleteImage($src)
    {
        if ($src) {
            $src = str_replace(SITE_BASE_URI, '', $src);
            unlink(ROOT . "/" . $src);
        }
    }

    private function _updateInterests($old_list, $new_list)
    {
        foreach ($old_list as $key => $item) {
            if (false !== $index = array_search($item, $new_list)) {
                unset($new_list[$index]);
            } else {
                User::deleteInterest($_POST['user_id'], $item);
            }
        }

        foreach ($new_list as $key => $item) {
            User::addInterest($_POST['user_id'], $item);
        }
    }

    private function _getInterestsArray($user)
    {
        $interests = array();

        foreach ($user as $row) {
            $interests[] = "'" . $row['interest_name'] . "'";
        }
        $string = implode(',', $interests);
        return $string;
    }

    private function _setPreferencesConditions($user_preferences)
    {
        switch ($user_preferences) {
            case 'heterosexual':
                $preferences = "AND (user_preferences = 'heterosexual' " .
                    "OR user_preferences = 'bisexual') " .
                    "AND NOT user_gender = '" . $this->user[0]['user_gender'] . "' ";
                break;
            case 'homosexual':
                $preferences = "AND (user_preferences = 'homosexual' " .
                    "OR user_preferences = 'bisexual') " .
                    "AND user_gender = '" . $this->user[0]['user_gender'] . "' ";
                break;
            case 'bisexual':
                $preferences = "AND user_preferences = 'bisexual' " .
                    "OR (user_preferences = 'heterosexual' AND NOT user_gender = '" .
                    $this->user[0]['user_gender'] . "') " .
                    "OR (user_preferences = 'homosexual' AND user_gender = '" .
                    $this->user[0]['user_gender'] . "') ";
                break;
            default:
                $preferences = "";
                break;
        }
        return $preferences;
    }

    private function _setSortParams($sort_type)
    {
        switch ($sort_type) {
            case 'order-distance':
                $sort_param = "distance ";
                break;
            case 'order-rating':
                $sort_param = "user_rating DESC ";
                break;
            case 'order-tags':
                $sort_param = "common_interests DESC ";
                break;
            default:
                $sort_param = "distance, common_interests DESC, user_rating DESC ";
                break;
        }
        return $sort_param;
    }

}


