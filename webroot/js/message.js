document.addEventListener("DOMContentLoaded", function () {
    var user = document.getElementsByClassName('login');
    for (var i = 0; i < user.length; i++) {
        var uri = this.baseURI.split('/');
        if (user[i].innerHTML !== uri[uri.length - 1]) {
            var messages = document.getElementsByClassName('text');
            messages[i].style.backgroundColor = "#a4b9d6";
            var message_item = document.getElementsByClassName('message-item');
            message_item[i].style.marginLeft = "30%";
            message_item[i].style.width = "50%";
            message_item[i].style.float = "none";
            message_item[i].style.marginRight = "10px";
            if (window.screen.width <= 480) {
                message_item[i].style.marginLeft = "10%";
                message_item[i].style.width = "60%";
                message_item[i].style.marginRight = "10px";
            }
            var photo = document.getElementsByClassName('message')[0].children[0].children[i].children[0];
            photo.style.float = "right";
            photo.style.paddingRight = "20px";
            if (window.screen.width <= 480) {
                photo.style.paddingRight = "20%";
            }
        }

    }

    var chatName = document.getElementsByClassName('chat-name');
    for (var j = 0; j < chatName.length; j++) {
        var uri2 = this.baseURI.split('/');
        if (chatName[j].innerHTML === uri2[uri2.length - 1]) {
            var item = document.getElementsByClassName('chat-item');
            item[j].style.boxShadow = " 0 0 10px 3px rgba(0,0,0,0.5)";
        }
    }

});
