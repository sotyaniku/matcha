function createRequest() {
    var request = false;

    if (window.XMLHttpRequest)
        request = new XMLHttpRequest();
    else
        alert ('Ajax is not supported!');
    return request;
}

function sendRequest(method, path, args, handler) {
    var request = createRequest();

    if (!request)
        return;

    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status === 200) {
            handler(request);
        }
    };

    if (method.toLowerCase() === "get" && args.length > 0)
        path += "?" + args;

    request.open(method, path, true);

    if (method.toLowerCase() === "post") {
        request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf8");
        request.send(args);
    }
    else
        request.send(null);
}