window.onload = function() {
    var form = document.querySelector('.edit-profile');
    var elems = form.elements;


    var validateForm = function() {
        var input = this;
        var msg = "";

        if (input.name === 'email') {
            if (!checkEmail(input.value)) {
                msg = "Please type a valid email";
            }
        }
        else if (input.name === 'firstname' && !checkName(input.value)) {
            msg = "Please check your name";
        }
        else if (input.name === 'lastname' && !checkName(input.value)) {
            msg = "Please check your lastname";
        }
        else if (input.name === 'age' && !checkAge(input.value)) {
            msg = "Please enter the number between 18 and 100";
        }
        else if (input.name === 'password' && !checkPass(input.value)) {
            msg = "Your password must be 8-50 characters long and must contain at least 1 number and 1 character";
        }
        else if (form.password.value !== form.password_2.value) {
            msg = "Please enter the same password twice";
        }
        else if (input.name === 'biography' && !checkBiography(input)) {
            // msg = "Your short biography must be no more than 255 symbols";
        }

        if (msg !== "") {
            input.className = "incorrect";
            writeMessage(msg, form);
        } else {
            input.className = "";
            removeMessage(form);
        }
    };

    for (var i = 0; i < elems.length; i++) {
        elems[i].addEventListener("blur", validateForm, false);
        elems[i].addEventListener("keyup", validateForm, false);
    }
};

function handleResponse(request) {
    console.log("handler: " + request.responseText);

    var response = request.responseText;

    if (response === "OK") {
        var icon = document.querySelector('.checkmark');
        icon.style.visibility = "visible";
        icon.style.opacity = 1;
        setTimeout(hideIcon, 2000, icon);
        clearPassword();
    }
    else if (response === "Email") {
        var form = document.querySelector('.edit-profile');
        document.querySelector('.error_msg').innerHTML = "A user with such email already exists";
        console.log(form.email.value);
        markAsIncorrect(form.email, true);
    }
}

function hideIcon(icon) {
    icon.style.visibility = "hidden";
    icon.style.opacity = 0;
}

function sendForm(form) {
    if (findIncorrectInputs(form))
        return false;

    var data = '';

    var elem = form.elements;

    for (var i = 0; i < elem.length; i++) {
        var name = elem[i].name;

        if (!(name === 'interest[]' && !elem[i].checked)) {
            data += name + "=" + elem[i].value + "&";
        }
    }

    sendRequest("POST", "/matcha/updateprofile", data, handleResponse);

    return false;
}

function markAsIncorrect(input, flag) {
    input.className = flag ? "incorrect" : '';
    //input.classList.toggle("incorrect", flag || flag == null);
}

function findIncorrectInputs(form) {
    var inputs = form.querySelectorAll('.incorrect');
    if (inputs.length === 0)
        return false;
    return true;
}

function removeMessage(form) {
    var msg = form.querySelectorAll('.error_msg');
    msg[0].innerHTML = "";
}

function writeMessage(msg, form) {
    var par = form.getElementsByClassName('error_msg');
    par[0].innerHTML = msg;
}

function checkName(name) {
    var matches = name.match(/^[a-zA-Z]+$/);
    return (matches !== null && name.length > 1 && name.length < 25);
}

function checkEmail(mail) {
    var matches = mail.match(/\S+@\S+/);
    return matches;
}

function checkPass(pass) {
    var numbers = /[0-9]/;
    var letters = /[a-zA-Z]/;
    return (pass.length > 7 && pass.length < 51 && numbers.test(pass) && letters.test(pass));
}

function checkAge(age) {
    var number = parseFloat(age);

    if (number == age && number >= 18 && number <= 100)
        return true;
    return false;
}

function clearPassword() {
    var form = document.querySelector('.edit-profile');

    form.password.value = '';
    form.password_2.value = '';
}

function checkBiography(text) {
    if (text.value.length <= 255)
        return true;

    var str = text.value;

    str = str.substring(0, 255);
    text.value = str;
}
