function handleResponse(request) {
    console.log("handler: " + request.responseText);

    var response = request.responseText;
    var circle = document.getElementById('status');
    var time = document.getElementById('last-time-online');

    if (response === "OK")
        circle.style.background = "#3c8e03";
    else {
        circle.style.background = "#d25f55";
        time.innerText = "(last seen: " + response + ")";
    }
}

window.onload = function () {
    var div = document.querySelector('.user-main-info');
    var login = div.getElementsByTagName('h3')[0].innerText;
    checkUserStatus(login);
}

function checkUserStatus(login) {
    var data = "login=" + login;
    setInterval(sendRequest("POST", "/matcha/status", data, handleResponse), 60000);
}

function uploadPhoto(input) {
    var img = document.querySelector(".user-photo");
    var reader = new FileReader();
    var hiddenInput = document.getElementById("photo-src");
    var icon = document.getElementById('save-icon');
    var src;

    reader.onload = function(theFile) {
        src = theFile.target.result;
        img.style.backgroundImage = "url(" + src + ")";
        icon.style.display = "block";
        hiddenInput.value = src;
    }
    reader.readAsDataURL(input.target.files[0]);
}



