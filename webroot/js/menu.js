function handleMenuResponse(request) {
    console.log("handler: " + request.responseText);

    var response = request.responseText;

    var bell = document.querySelector('.notification').getElementsByTagName('i')[0];

    if (response)
        bell.innerText = response;
    else
        bell.innerText = 0;
}

document.addEventListener("DOMContentLoaded", ready);

function ready () {
    var div = document.getElementById('user-id');
    var id = div.value;
    checkNotificationNum(id);
}

function checkNotificationNum(id) {
    var data = "id=" + id;
    setInterval(sendRequest("POST", "/matcha/notification", data, handleMenuResponse), 60000);
}

function showList(menuItem) {
    var submenu = menuItem.querySelector('ul');
    var triangles = menuItem.querySelectorAll('b');
    submenu.style.display = "block";
    for (var i = 0; i < triangles.length; i++)
        triangles[i].style.display = "block";
}

function hideList(menuItem) {
    var submenu = menuItem.querySelector('ul');
    var triangles = menuItem.querySelectorAll('b');
    submenu.style.display = "none";
    for (var i = 0; i < triangles.length; i++)
        triangles[i].style.display = "none";
}

document.querySelector('body').addEventListener("click", function showEventListener(event) {
    var menuIcons = document.querySelectorAll('.has-list');
    for (var i = 0; i < menuIcons.length; i++) {
        hideList(menuIcons[i]);
    }
    if (event.target.parentNode.classList.contains('has-list'))
        showList(event.target.parentNode);
});
