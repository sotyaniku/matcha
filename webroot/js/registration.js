window.onload = function() {
    getLocation();
}

function findCountry(coordinates, callback) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({'location': coordinates}, function(results, status) {
        var country = document.getElementById('country');

        if (status === 'OK') {
            var len = results.length;
            country.value = results[len - 1].address_components[0].short_name;
            callback(country.value);
            return;
        }
        callback(false);
    });
}

function handleResponse(request) {
    console.log("handler: " + request.responseText);

    var response = request.responseText;
    var regForm = document.querySelector('.reg');
    var logForm = document.querySelector('.log-in');
    var passForm = document.querySelector('.send-pass');

    if (response === "OK") {
        clearForm(regForm);
        document.getElementById('r2').setAttribute("checked", "");
        document.getElementById('r1').removeAttribute("checked");
        document.querySelector('.error_msg').innerHTML = "";
    }
    else if (response === "Username") {
        document.querySelector('.error_msg').innerHTML = "A user with such username already exists";
        markAsIncorrect(regForm.username, regForm);
        }
    else if (response === "Email") {
        document.querySelector('.error_msg').innerHTML = "A user with such email already exists";
        markAsIncorrect(regForm.email, regForm);
    }
    else if (response === "Login") {
        logForm.querySelector('.error_msg').innerHTML = "Incorrect Username/Password combination.<br>"
            + "Forgot your password? Click <a href=\"sendpass\">here</a> for update";
    }
    else if (response === "Sendpass error") {
        passForm.querySelector('.error_msg').innerHTML = "A user with such email doesn't exist";
        markAsIncorrect(passForm.email, passForm);
    }
    else if (response === "Sendpass success") {
        window.location = '/matcha/resetpass';
    }
    else if (response === "Main") {
        window.location = '/matcha/';
    }
}

function clearForm(form) {
    var field = form.getElementsByTagName('input');

    for (var i = 0, len = field.length; i < len; i++) {
        if (field[i].type !== "submit") {
            field[i].value = "";
        }
    }
}

function sendForm(form) {
    if (!validateForm(form))
        return false;

    var data;

    if (form.className === "reg") {

        data = "username=" + form.username.value + "&email=" + form.email.value
               + "&firstname=" + form.firstname.value  + "&lastname=" + form.lastname.value
               + "&password=" + form.password.value  + "&gender=" + form.gender.value
               + "&preferences=" + form.preferences.value  + "&register=Register";
        getLocation();
        if (document.getElementById('latitude').value !== null) {
            data += "&latitude=" + form.latitude.value + "&longitude=" + form.longitude.value
                    + "&country=" + form.country.value;
        }

        sendRequest("POST", "/matcha/register", data, handleResponse);
    }
    else if (form.className === "log-in") {
        data = "username=" + form.username.value + "&password=" + form.password.value
        + "&log-in=Log in";
        sendRequest("POST", "/matcha/login", data, handleResponse);
    }
    else if (form.className === "send-pass") {
        data = "email=" + form.email.value + "&sendpass=Sendpass";
        sendRequest("POST", "/matcha/sendpass", data, handleResponse);
    }
    else
        return false;
    return false;
}

function errorLocation() {
    console.log("geolocation forbidden");
}

function successLocation(position) {
    var x = position.coords.latitude;
    var y = position.coords.longitude;

    console.log(x + "/" + y);

    var latitude = document.getElementById('latitude');
    var longitude = document.getElementById('longitude');

    latitude.value = x;
    longitude.value = y;

    var newLatLng = new google.maps.LatLng(x, y);

    findCountry(newLatLng, function(res) {
        var country = document.getElementById('country');

        if (!res) {
            country.value = "BY";
        }
        else {
            country.value = res;
        }
    });
}

function getLocation() {
    if (navigator.geolocation)
        navigator.geolocation.getCurrentPosition(successLocation, errorLocation);
}


function validateForm(form) {
    var msg = "";
    var valid = true;
    var field = form.getElementsByTagName('input');

    var i = 0;
    while (i < field.length) {
        if (field[i].type === "hidden")
            i++;

        else if (field[i].type !== "hidden" && field[i].value === "") {
            if (form.className === "send-pass")
                msg = "Please enter your email";
            else
                msg = "Please fill in all the fields";
            valid = false;
            markAsIncorrect(field[i], form);
        }
        i++;
    }

    if (valid === false) {
        writeMessage(msg, form);
        return valid;
    }
    else if (form.className === "send-pass") {
        if (!checkEmail(form.email.value)) {
            msg = "Please type a valid email";
            markAsIncorrect(form.email, form);
        }
    }
    else if (form.className === "update-pass") {
        console.log("here");
        if (!checkPass(form.password.value)) {
            msg = "Your password must be 8-50 characters long and must contain at least 1 number and 1 character";
            markAsIncorrect(form.password, form);
        }
        else if (form.password.value !== form.password_2.value) {
            msg = "Please enter the same password twice";
            markAsIncorrect(form.password_2, form);
        }
    }
    else if (!checkLogin(form.username.value)) {
        msg = "Username must be 3-14 characters long and must contain only letters [a-z] and/or numbers";
        markAsIncorrect(form.username, form);
    }
    else if (form.className === "reg" && !checkEmail(form.email.value)) {
        msg = "Please type a valid email";
        markAsIncorrect(form.email, form);
    }
    else if (form.className === "reg" && !checkName(form.firstname.value)) {
        msg = "Please check your name";
        markAsIncorrect(form.firstname, form);
    }
    else if (form.className === "reg" && !checkName(form.lastname.value)) {
        msg = "Please check your lastname";
        markAsIncorrect(form.lastname, form);
    }
    else if (!checkPass(form.password.value)) {
        msg = "Your password must be 8-50 characters long and must contain at least 1 number and 1 character";
        markAsIncorrect(form.password, form);
    }
    else if (form.className === "reg" && form.password.value !== form.password_2.value) {
        msg = "Please enter the same password twice";
        markAsIncorrect(form.password_2, form);
    }

    if (msg !== "") {
        valid = false;
        writeMessage(msg, form);
    }
    return valid;
}

function markAsIncorrect(input, form) {
    input.className = "incorrect";
    input.addEventListener("keypress", function focusEventListener() {
        this.className = "";
        if (!findIncorrectInputs(form))
            removeMessage(form);
        this.removeEventListener("keypress", focusEventListener, false);
    }, false);
}

function findIncorrectInputs(form) {
    var inputs = form.querySelectorAll('.incorrect');
    if (inputs.length === 0)
        return false;
    return true;
}

function removeMessage(form) {
    var msg = form.querySelectorAll('.error_msg');
    msg[0].innerHTML = "";
}

function writeMessage(msg, form) {
    var par = form.getElementsByClassName('error_msg');
    par[0].innerHTML = msg;
}

function checkName(name) {
    var matches = name.match(/^[a-zA-Z]+$/);
    return (matches !== null && name.length > 1 && name.length < 25);
}

function checkLogin(name) {
    var match = name.match(/^[a-zA-Z0-9]+$/);
    return (match !== null && name.length > 2 && name.length < 15);
}

function checkEmail(mail) {
    var matches = mail.match(/\S+@\S+/);
    return matches;
}

function checkPass(pass) {
    var numbers = /[0-9]/;
    var letters = /[a-zA-Z]/;
    return (pass.length > 7 && pass.length < 51 && numbers.test(pass) && letters.test(pass));
}