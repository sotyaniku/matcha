function findCity(coordinates, callback) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({'location': coordinates}, function(results, status) {
        var city = document.getElementById('city');

        if (status === 'OK') {
            //console.log(results);
            var country = document.getElementById('country');
            var len = results.length;
            country.value = results[len - 1].address_components[0].short_name;
            // console.log(country.value);

            for (var i = 0; i < results.length; i++) {
                if (results[i].types[0] === "locality") {

                    city.value = results[i].address_components[0].long_name;
                    callback(true);
                    return;
                }
                if (results[i].types[0] === "administrative_area_level_1") {

                    city.value = results[i].address_components[0].long_name;
                    callback(true);
                    return;
                }
            }
            city.value = results[2].formatted_address;
            callback(true);
            return;
        }
        city.value = "Choose your location on the map";
        callback(false);
    });
}

function initialize() {
    var x = document.getElementById('latitude');
    var y = document.getElementById('longitude');

    var lat = x.value ? x.value : "<?php echo $data[0]['user_latitude']; ?>";
    var lon = y.value ? y.value : "<?php echo $data[0]['user_longitude']; ?>";
    var latlng = new google.maps.LatLng(lat, lon);

    var options = {
        zoom: 8,
        center: latlng
    }

    var map = new google.maps.Map(document.getElementById("map"), options);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable: true
    });

    google.maps.event.addListener(marker, 'dragend', function(event) {
        //console.log("lat: " + event.latLng.lat() + ", lon: " + event.latLng.lng());
        var x = document.getElementById('latitude');
        var y = document.getElementById('longitude');

        var newLatLng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

        findCity(newLatLng, function(res) {
            if (res) {
                x.value = event.latLng.lat();
                y.value = event.latLng.lng();
            }
        });
    });

    findCity(latlng, function f() {});
}

function uploadPhoto(input) {
    var img = document.querySelector(".img");
    var reader = new FileReader();
    var hiddenInput = document.getElementById("photo-src");
    var src;

    reader.onload = function(theFile) {
        src = theFile.target.result;
        img.style.backgroundImage = "url(" + src + ")";
        hiddenInput.value = src;
    }
    reader.readAsDataURL(input.target.files[0]);
}

function showMap() {
    var map = document.getElementById('map');
    var closeBtn = document.getElementById('close-btn');
    var bg = document.getElementById('container');

    map.style.display = "block";
    closeBtn.style.display= "block";
    bg.style.display = "block";
    initialize();
}

function showInterests() {
    var closeBtn = document.getElementById('close-list-btn');
    var bg = document.getElementById('white-container');

    closeBtn.style.display= "block";
    bg.style.display = "block";
}

function hideMap() {
    var map = document.getElementById('map');
    var closeBtn = document.getElementById('close-btn');
    var bg = document.getElementById('container');

    map.style.display = "none";
    closeBtn.style.display = "none";
    bg.style.display = "none";
}

function hideInterests() {
    var closeBtn = document.getElementById('close-list-btn');
    var bg = document.getElementById('white-container');

    rewriteInterests();
    closeBtn.style.display = "none";
    bg.style.display = "none";
}

function rewriteInterests() {
    var list = document.getElementById('white-container');
    var interests = list.getElementsByTagName('input');
    var label;

    clearInterestList();

    for (var i = 0; i < interests.length; i++) {
        if (interests[i].checked) {
            label = interests[i].nextElementSibling;
            addInterestToList(label.innerText);
        }
    }
}

function clearInterestList() {
    var list = document.querySelector('.interest').getElementsByTagName('ul');
    var interests = list[0].getElementsByTagName('li');

    var i = 0;
    while (interests[i]) {
        interests[i].parentNode.removeChild(interests[0]);
    }
}

function addInterestToList(interest) {
    var list = document.querySelector('.interest').getElementsByTagName('ul');
    var newItem = document.createElement('li');
    list[0].appendChild(newItem);
    newItem.innerHTML = interest;
}


