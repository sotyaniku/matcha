<?php

require_once ROOT . '/model/Db.php';

class Session {
    public static function getSession($user_id) {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_sessions WHERE user_id = :user_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $session = $sth->fetch();
        return $session;
    }

    public static function updateSession($user_id, $session_id, $time) {
        $db = Db::getConnection();

        $sql = "UPDATE tbl_sessions SET session_id = :session_id, last_time = :last_time " .
               "WHERE user_id = :user_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "session_id" => $session_id,
            "last_time" => $time
        ));
    }

    public static function setSession($user_id, $session_id, $last_time) {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_sessions (user_id, session_id, last_time) " .
               "VALUES (:user_id, :session_id, :last_time)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "session_id" => $session_id,
            "last_time" => $last_time
        ));
    }

    public static function deleteOldSessions($check_time) {
        $db = Db::getConnection();

        $sql = "DELETE FROM tbl_sessions WHERE last_time < :check_time";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "check_time" => $check_time
        ));
    }
}