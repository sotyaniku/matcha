<?php

require_once ROOT . '/model/Db.php';

class User
{
    public static function getUserByEmail($email)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_mail = :email";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "email" => $email
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();
        return $user;
    }

    public static function getUserById($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();
        return $user;
    }

    public static function getUserByLogin($login)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_user WHERE user_login = :login";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $user = $sth->fetch();
        return $user;
    }

    public static function getUserAllInfoByLogin($login)
    {
        $db = Db::getConnection();

        $sql = "SELECT tbl_user.*, tbl_interests.interest_name FROM tbl_user " .
            "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
            "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id=tbl_interests.interest_id " .
            "WHERE tbl_user.user_login = :login";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function getUserPhotosByLogin($login)
    {
        $db = Db::getConnection();

        $sql = "SELECT tbl_user.*, tbl_photos.photo_id, tbl_photos.photo_src FROM tbl_user " .
            "INNER JOIN tbl_photos ON tbl_user.user_id = tbl_photos.user_id " .
            "WHERE tbl_user.user_login = :login";


        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function InterestList()
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_interests";
        $sth = $db->query($sql);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $sth->fetch())
            $interest[$i++] = $row;
        return $interest;
    }

    public static function getInterestsList($login)
    {
        $db = Db::getConnection();

        $sql = "SELECT tbl_user.user_login, tbl_interests.* FROM tbl_user " .
            "JOIN tbl_hashtag ON tbl_user.user_id=tbl_hashtag.user_id " .
            "AND tbl_user.user_login = :login " .
            "RIGHT JOIN tbl_interests ON tbl_hashtag.interest_id=tbl_interests.interest_id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while ($row = $sth->fetch())
            $interest[$i++] = $row;
        return $interest;
    }

    public static function getInterestsById($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT interest_id FROM tbl_hashtag WHERE user_id = :id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_NUM);
        $interest = array();
        $i = 0;
        while ($row = $sth->fetch())
            $interest[$i++] = $row[0];
        return $interest;
    }

    public static function deleteInterest($user_id, $interest_id)
    {
        $db = Db::getConnection();

        $sql = "DELETE from tbl_hashtag WHERE user_id = :user_id AND interest_id = :interest_id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "interest_id" => $interest_id
        ));
    }

    public static function addInterest($user_id, $interest_id)
    {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_hashtag (user_id, interest_id) VALUES (:user_id, :interest_id)";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "interest_id" => $interest_id
        ));
    }

    public static function addUserToDb($login, $firstname, $lastname, $email, $password, $gender, $preferences, $latitude, $longitude, $country)
    {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_user " .
            "(user_login, user_firstname, user_lastname, user_mail, " .
            "user_password, user_gender, user_preferences, user_latitude, user_longitude, user_country) " .
            "VALUES (:login, :firstname, :lastname, :email, :password, :gender, :preferences, :latitude, :longitude, :country)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "login" => $login,
            "password" => $password,
            "email" => $email,
            "firstname" => $firstname,
            "lastname" => $lastname,
            "gender" => $gender,
            "preferences" => $preferences,
            "latitude" => $latitude,
            "longitude" => $longitude,
            "country" => $country
        ));
    }

    public static function updateUserInfo($id, $firstname, $lastname, $email, $gender, $preferences, $age, $latitude, $longitude, $biography, $country)
    {
        $db = Db::getConnection();

        $sql = "UPDATE tbl_user SET " .
            "user_firstname = :firstname, user_lastname = :lastname, user_mail = :email, user_gender = :gender, " .
            "user_preferences = :preferences, user_age = :age, user_latitude = :latitude, " .
            "user_longitude = :longitude, user_biography = :biography, user_country = :country " .
            "WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "email" => $email,
            "firstname" => $firstname,
            "lastname" => $lastname,
            "gender" => $gender,
            "preferences" => $preferences,
            "latitude" => $latitude,
            "longitude" => $longitude,
            "age" => $age,
            "biography" => $biography,
            "country" => $country
        ));
    }

    public static function updateUserPassword($id, $password)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_user SET user_password = :password WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "password" => $password
        ));
    }

    public static function updateUserPhoto($id, $url)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_user SET user_profilephoto = :url WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "url" => $url
        ));
    }

    public static function addPhoto($id, $url)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO tbl_photos (user_id, photo_src) VALUES (:id, :url)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "url" => $url
        ));
    }

    public static function findLike($from, $to)
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM tbl_likes WHERE visitor_id = :from AND profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $like = $sth->fetch();
        return $like;
    }

    public static function addLike($from, $to, $time)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO tbl_likes (visitor_id, profile_id, like_time) VALUES (:from, :to, :time)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to,
            "time" => $time
        ));
    }

    public static function changeLike($from, $to, $status, $seen, $time)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_likes SET like_status = :status, like_seen = :seen, like_time = :time " .
            "WHERE visitor_id = :from AND profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to,
            "status" => $status,
            "time" => $time,
            "seen" => $seen
        ));
    }

    public static function updateSeenLikes($to, $status)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_likes SET like_seen = :status WHERE profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "to" => $to,
            "status" => $status
        ));
    }

    public static function getLikeStatus($visitor_id, $profile_id)
    {
        $db = Db::getConnection();

        $sql = "SELECT like_status FROM tbl_likes WHERE visitor_id = :visitor_id AND profile_id = :profile_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "visitor_id" => $visitor_id,
            "profile_id" => $profile_id
        ));

        $sth->setFetchMode(PDO::FETCH_NUM);
        $num = $sth->fetch();
        return $num[0];
    }

    public static function findVisit($from, $to)
    {
        $db = Db::getConnection();
        $sql = "SELECT * FROM tbl_visits WHERE visitor_id = :from AND profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $like = $sth->fetch();
        return $like;
    }

    public static function addVisit($from, $to, $time)
    {
        $db = Db::getConnection();
        $sql = "INSERT INTO tbl_visits (visitor_id, profile_id, visit_time) VALUES (:from, :to, :time)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to,
            "time" => $time
        ));
    }

    public static function changeVisit($from, $to, $status, $time)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_visits SET visit_seen = :status, visit_time = :time " .
            "WHERE visitor_id = :from AND profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "from" => $from,
            "to" => $to,
            "status" => $status,
            "time" => $time
        ));
    }

    public static function updateSeenVisits($to, $status)
    {
        $db = Db::getConnection();
        $sql = "UPDATE tbl_visits SET visit_seen = :status WHERE profile_id = :to";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "to" => $to,
            "status" => $status
        ));
    }

    public static function showHistory($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT tbl_user.user_login, tbl_user.user_profilephoto, tbl_visits.visit_time FROM tbl_visits INNER JOIN tbl_user ON tbl_user.user_id = profile_id WHERE visitor_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function showVisits($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT tbl_user.user_login, tbl_user.user_profilephoto, tbl_visits.visit_time FROM tbl_visits INNER JOIN tbl_user ON tbl_user.user_id = visitor_id WHERE profile_id = :id ORDER BY visit_time DESC";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function showLikes($id)
    {
        $db = Db::getConnection();
        $sql = "SELECT tbl_user.user_login, tbl_user.user_profilephoto, tbl_likes.like_time FROM tbl_likes INNER JOIN tbl_user ON tbl_user.user_id = visitor_id WHERE profile_id = :id ORDER BY like_time DESC";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function showNotifications($id, $status)
    {
        $db = Db::getConnection();
        $sql = "SELECT tbl_user.user_login, tbl_user.user_profilephoto, " .
            "tbl_visits.type, tbl_visits.visit_time, 0 AS like_status " .
            "FROM tbl_visits INNER JOIN tbl_user ON tbl_user.user_id = visitor_id " .
            "WHERE profile_id = :id AND visit_seen = :status " .
            "UNION ALL " .
            "SELECT tbl_user.user_login, tbl_user.user_profilephoto, tbl_likes.type, tbl_likes.like_time, tbl_likes.like_status FROM tbl_likes " .
            "INNER JOIN tbl_user ON tbl_user.user_id = visitor_id " .
            "WHERE profile_id = :id AND like_seen = :status ORDER BY visit_time DESC";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "status" => $status
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function countNotifications($id, $status)
    {
        $db = Db::getConnection();
        $sql = "SELECT count(*) FROM " .
            "(SELECT visitor_id FROM tbl_visits WHERE profile_id = :id " .
            "AND visit_seen = :status " .
            "UNION ALL " .
            "SELECT visitor_id FROM tbl_likes WHERE profile_id = :id AND like_seen = :status) " .
            " AS tbl";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id,
            "status" => $status
        ));
        $sth->setFetchMode(PDO::FETCH_NUM);
        $num = $sth->fetch();
        return $num;
    }

    public static function showSuggestions($user, $interests, $sort_param, $pref_conditions)
    {
        $db = Db::getConnection();

        $sql = "SELECT user_login, user_profilephoto, user_rating,
                ceil(distance * 0.1) AS distance, count(interest_name) AS common_interests FROM " .
            "(SELECT user_id, user_login, user_rating, user_profilephoto,
                (6371 * acos(cos(radians(:lat))
                * cos(radians(user_latitude))
                * cos(radians(user_longitude)
                - radians(:lon)) + sin(radians(:lat))
                * sin(radians(user_latitude)))) AS distance
                FROM tbl_user " .
            "WHERE user_country = :country AND NOT user_id = :id " .
            $pref_conditions .
            "HAVING distance < 400) AS t1 " .
            "LEFT JOIN " .
            "(SELECT tbl_user.user_id, interest_name FROM tbl_user " .
            "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
            "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id = tbl_interests.interest_id " .
            "WHERE interest_name IN ($interests)) AS t2 " .
            "ON t1.user_id = t2.user_id " .
            "LEFT JOIN tbl_blocked ON tbl_blocked.user_id = :id AND tbl_blocked.profile_id = t1.user_id ".
            "WHERE tbl_blocked.profile_id IS NULL " .
            "GROUP BY user_login, user_rating, user_profilephoto, distance " .
            "ORDER BY " . $sort_param . "LIMIT 0, 20";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $user['user_id'],
            "country" => $user['user_country'],
            "lat" => $user['user_latitude'],
            "lon" => $user['user_longitude']
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch()) {
            $user[$i++] = $row;
        }
        return $user;
    }

    public static function showFilteredSuggestions($user, $interests, $sort_param, $filter_param, $preferences)
    {
        $db = Db::getConnection();

        $sql = "SELECT t3.person_id, user_login, user_profilephoto, user_rating, 
                distance, common_interests FROM " .
            "(SELECT t1.user_id AS person_id, user_login, user_profilephoto, user_rating, 
                ceil(dist * 0.1) AS distance, count(interest_name) AS common_interests FROM " .
            "(SELECT user_id, user_login, user_rating, user_profilephoto,
                (6371 * acos(cos(radians(:lat))
                * cos(radians(user_latitude))
                * cos(radians(user_longitude)
                - radians(:lon)) + sin(radians(:lat))
                * sin(radians(user_latitude)))) AS dist
                FROM tbl_user " .
            "WHERE user_country = :country AND NOT user_id = :id " .
            $preferences .
            "HAVING dist < 400) AS t1 " .
            "LEFT JOIN " .
            "(SELECT tbl_user.user_id, interest_name FROM tbl_user " .
            "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
            "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id = tbl_interests.interest_id " .
            "WHERE interest_name IN ($interests)) " .
            "AS t2 " .
            "ON t1.user_id = t2.user_id " .
            "WHERE user_rating >= " . $filter_param['rating'] . " " .
            "AND ceil(dist * 0.1) <= " . $filter_param['distance'] . " " .
            "GROUP BY t1.user_id, user_login, user_rating, user_profilephoto, distance " .
            "ORDER BY " . $sort_param . "LIMIT 0, 20) AS t3 " .
            "LEFT JOIN tbl_blocked ON tbl_blocked.user_id = :id AND tbl_blocked.profile_id = person_id ".
            "WHERE tbl_blocked.profile_id IS NULL " .
            "AND common_interests >= " . $filter_param['tags'];


        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $user['user_id'],
            "country" => $user['user_country'],
            "lat" => $user['user_latitude'],
            "lon" => $user['user_longitude']
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function searchPeople($user, $sort_param, $pref_conditions, $in_list, $param, $filter_params) {
    $db = Db::getConnection();

    $sql = "SELECT 
    user_login, 
    user_rating,
    user_profilephoto,
    user_age,
    distance,
    count(interest_name) AS common_interests 
    FROM (
        SELECT 
            user_login, 
            user_age, 
            user_profilephoto, 
            user_rating,
            ceil(dist) AS distance, 
            interest_name 
        FROM " .
           "(SELECT 
                user_id, 
                user_login, 
                user_age, 
                user_rating, 
                user_profilephoto, 
                user_preferences, 
                (6371 * acos(cos(radians(:lat))
                * cos(radians(user_latitude))
                * cos(radians(user_longitude)
                - radians(:lon)) + sin(radians(:lat))
                * sin(radians(user_latitude)))) AS dist
            FROM tbl_user " .
           "WHERE NOT user_id = :id " .
           $pref_conditions .
           ") AS t1 LEFT JOIN " .
           "(SELECT tbl_user.user_id, interest_name FROM tbl_user " .
           "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
           "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id = tbl_interests.interest_id " .
           $in_list . ") AS t2 " .
           "ON t1.user_id = t2.user_id " .
           "LEFT JOIN tbl_blocked ON tbl_blocked.user_id = :id AND tbl_blocked.profile_id = t1.user_id ".
           "WHERE tbl_blocked.profile_id IS NULL " .
           "AND t1.dist BETWEEN :distance_from AND :distance_to) AS t3 " .
           "WHERE user_rating >= " . $filter_params['rating'] . " " .
           "AND distance <= " . $filter_params['distance'] . " " .
           "AND user_rating BETWEEN :rating_from AND :rating_to " .
           "AND user_age BETWEEN :age_from AND :age_to " .
           "GROUP BY user_login, user_rating, user_profilephoto, user_age, distance " .
           "HAVING common_interests >= " . $filter_params['tags'] ." " .
           "ORDER BY " . $sort_param . "LIMIT 0, 20";

    $sth = $db->prepare($sql);
    $sth->execute(array(
        "id" => $user['user_id'],
        "country" => $user['user_country'],
        "lat" => $user['user_latitude'],
        "lon" => $user['user_longitude'],
        "distance_from" => $param['distance_from'],
        "distance_to" => $param['distance_to'],
        "age_from" => $param['age_from'],
        "age_to" => $param['age_to'],
        "rating_from" => $param['rating_from'],
        "rating_to" => $param['rating_to']
    ));
    $sth->setFetchMode(PDO::FETCH_ASSOC);
    $i = 0;
    $user = array();
    while ($row = $sth->fetch())
        $user[$i++] = $row;
    return $user;
}

    public static function searchAllAgesPeople($user, $sort_param, $pref_conditions, $in_list, $param, $filter_params)
    {
        $db = Db::getConnection();

        $sql = "SELECT 
    user_login, 
    user_rating,
    user_profilephoto,
    user_age,
    distance,
    count(interest_name) AS common_interests 
    FROM (
        SELECT 
            user_login, 
            user_age, 
            user_profilephoto, 
            user_rating,
            ceil(dist) AS distance, 
            interest_name 
        FROM " .
               "(SELECT 
                user_id, 
                user_login, 
                user_age, 
                user_rating, 
                user_profilephoto, 
                user_preferences, 
                (6371 * acos(cos(radians(:lat))
                * cos(radians(user_latitude))
                * cos(radians(user_longitude)
                - radians(:lon)) + sin(radians(:lat))
                * sin(radians(user_latitude)))) AS dist
            FROM tbl_user " .
               "WHERE NOT user_id = :id " .
               $pref_conditions .
               ") AS t1 LEFT JOIN " .
               "(SELECT tbl_user.user_id, interest_name FROM tbl_user " .
               "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
               "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id = tbl_interests.interest_id " .
               $in_list . ") AS t2 " .
               "ON t1.user_id = t2.user_id " .
               "LEFT JOIN tbl_blocked ON tbl_blocked.user_id = :id AND tbl_blocked.profile_id = t1.user_id ".
               "WHERE tbl_blocked.profile_id IS NULL " .
               "AND t1.dist BETWEEN :distance_from AND :distance_to) AS t3 " .
               "WHERE user_rating >= " . $filter_params['rating'] . " " .
               "AND distance <= " . $filter_params['distance'] . " " .
               "AND user_rating BETWEEN :rating_from AND :rating_to " .
               "GROUP BY user_login, user_rating, user_profilephoto, user_age, distance " .
               "HAVING common_interests >= " . $filter_params['tags'] ." " .
               "ORDER BY " . $sort_param . "LIMIT 0, 20";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $user['user_id'],
            "country" => $user['user_country'],
            "lat" => $user['user_latitude'],
            "lon" => $user['user_longitude'],
            "distance_from" => $param['distance_from'],
            "distance_to" => $param['distance_to'],
            "rating_from" => $param['rating_from'],
            "rating_to" => $param['rating_to']
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function searchFilteredAllAgesPeople($user, $sort_param, $pref_conditions, $in_list, $param, $filtered_param)
    {
        $db = Db::getConnection();
        echo $in_list;

        $sql = "SELECT user_login, user_age, user_profilephoto, user_rating,
                ceil(dist) AS distance, count(interest_name) AS common_interests FROM " .
               "(SELECT user_id, user_login, ifnull(user_age, 18) AS user_age, user_rating, 
               user_profilephoto, user_preferences, 
                (6371 * acos(cos(radians(:lat))
                * cos(radians(user_latitude))
                * cos(radians(user_longitude)
                - radians(:lon)) + sin(radians(:lat))
                * sin(radians(user_latitude)))) AS dist
                FROM tbl_user " .
               "WHERE user_rating BETWEEN :rating_from AND :rating_to " .
               "AND NOT user_id = :id " .
               $pref_conditions .
               "HAVING dist BETWEEN :distance_from AND :distance_to) AS t1 " .
               "LEFT JOIN " .
               "(SELECT tbl_user.user_id, interest_name FROM tbl_user " .
               "LEFT JOIN tbl_hashtag ON tbl_user.user_id = tbl_hashtag.user_id " .
               "LEFT JOIN tbl_interests ON tbl_hashtag.interest_id = tbl_interests.interest_id " .
               $in_list . ") AS t2 " .
               "ON t1.user_id = t2.user_id " .
               "GROUP BY user_login, user_rating, user_profilephoto, user_age, distance " .
               "ORDER BY " . $sort_param . "LIMIT 0, 20";

        echo $sql;

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $user['user_id'],
            "country" => $user['user_country'],
            "lat" => $user['user_latitude'],
            "lon" => $user['user_longitude'],
            "distance_from" => $param['distance_from'],
            "distance_to" => $param['distance_to'],
            "rating_from" => $param['rating_from'],
            "rating_to" => $param['rating_to']
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        $user = array();
        while ($row = $sth->fetch())
            $user[$i++] = $row;
        return $user;
    }

    public static function getMessages($user_id, $lover_login)
    {

        $db = Db::getConnection();
        $chat = array();

        $sql = "SELECT m.user_id, m.lover_id, m.message_time, m.message_text, u.user_login, 
            u.user_profilephoto, l.user_profilephoto as lvrPhoto FROM tbl_messages m " .
            "LEFT JOIN tbl_user u ON m.user_id = u.user_id " .
            "LEFT JOIN tbl_user l ON m.lover_id = l.user_id " .
            "WHERE m.user_id =:lover_id AND m.lover_id =:user_id OR m.user_id = :user_id AND m.lover_id = :lover_id";

        $result = $db->prepare($sql);
        $result->execute(array('user_id' => $user_id, 'lover_id' => $lover_login));
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while($row = $result->fetch()){
            $chat[$i++] = $row;
        }

        return $chat;

    }

    public static function getAllConversations($user_id)
    {
        $db = Db::getConnection();
        $conversations = array();

        $sql = "SELECT p.*, b.MaxTime FROM tbl_user p INNER JOIN ( SELECT a.lover_id, MAX(a.message_time) as MaxTime ".
            "FROM (SELECT user_id , lover_id, message_time, message_text FROM tbl_messages WHERE user_id = :user_id UNION ".
            "SELECT lover_id, user_id, message_time, message_text FROM tbl_messages WHERE lover_id = :user_id ) a ".
            "GROUP BY a.lover_id ) b ON p.user_id = b.lover_id ORDER BY b.MaxTime DESC";

        $result = $db->prepare($sql);
        $result->execute(array('user_id' => $user_id));
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $conversations = $result->fetchAll();
        return $conversations;
    }

    public static function writeMessage($user_id, $partner_id, $time, $text)
    {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_messages (user_id, lover_id, message_time, message_text ) 
              VALUES (:user_id, :lover_id, :time, :text)";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "lover_id" => $partner_id,
            "time" => $time,
            "text" => $text
        ));
    }

    public static function reportFakeUser($id)
    {
        $db = Db::getConnection();

        $sql = "UPDATE tbl_user SET user_fake = user_fake + 1 WHERE user_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));

    }

    public static function checkIfIsBlocked($user_id, $profile_id) {
        $db = Db::getConnection();

        $sql = "SELECT count(*) FROM tbl_blocked WHERE user_id = :user_id AND profile_id = :profile_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "profile_id" => $profile_id
        ));

        $sth->setFetchMode(PDO::FETCH_NUM);
        $num = $sth->fetch();
        return $num[0];
    }

    public static function blockUser($user_id, $profile_id)
    {
        $db = Db::getConnection();

        $sql = "INSERT INTO tbl_blocked (user_id, profile_id) VALUES (:user_id, :profile_id)";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "profile_id" => $profile_id
        ));

    }

    public static function getRating($user_id) {
        $db = Db::getConnection();

        $sql = "SELECT user_rating FROM tbl_user WHERE user_id = :user_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id
        ));

        $sth->setFetchMode(PDO::FETCH_NUM);
        $num = $sth->fetch();
        return $num[0];
    }

    public static function updateRating($user_id, $value)
    {
        $db = Db::getConnection();

        $sql = "UPDATE tbl_user SET user_rating = user_rating + :value WHERE user_id = :user_id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "user_id" => $user_id,
            "value" => $value
        ));

    }
}




