<?php
class Db
{
    public static $pdo;

	public static function getConnection()
	{
        if (!self::$pdo)
        {
            self::$pdo = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
        }
        return self::$pdo;
	}
}
