<?php

require_once ROOT . '/model/Db.php';

class Photo
{
    public static function removePhoto($id)
    {
        $db = Db::getConnection();
        $sql = "DELETE FROM tbl_photos WHERE photo_id = :id";
        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
    }

    public static function getPhotoById($id)
    {
        $db = Db::getConnection();

        $sql = "SELECT * FROM tbl_photos WHERE photo_id = :id";

        $sth = $db->prepare($sql);
        $sth->execute(array(
            "id" => $id
        ));
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $photo = $sth->fetch();
        return $photo;
    }

}