<?php
$avatar = (!empty($data[0]['user_profilephoto'])) ? $data[0]['user_profilephoto'] : "/matcha/webroot/images/avatar.png";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matcha</title>
    <link rel="stylesheet" type="text/css" href="/matcha/webroot/css/mainstyle.css">
    <link rel="stylesheet" type="text/css" href="/matcha/webroot/css/mobile.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Indie+Flower">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

</head>
<body>
   <div>
       <div class="header">
           <div class="center">
               <div class="logo">
                   <a href="/matcha/"><h1>Matcha</h1></a>
               </div>
               <div class="self header-icon has-list">
                   <div style="background-image: url(<?php echo $avatar; ?>);"></div>
                   <ul>
                       <li><a href="/matcha/<?php echo $_SESSION['logged_user'];?>">My page</a></li>
                       <li><a href="/matcha/likes">Profile likes</a></li>
                       <li><a href="/matcha/visitors">Profile visitors</a></li>
                       <li><a href="/matcha/history">History</a></li>
                       <li><a href="/matcha/profile">Edit profile</a></li>
                       <li><a href="/matcha/logout">Log out</a></li>
                   </ul>
                   <b class="arrow-border"></b>
                   <b class="arrow"></b>
               </div>
               <div class="notification header-icon">
                   <a href="/matcha/notifications"><img src="/matcha/webroot/images/bell.png"><i></i></a>
               </div>
               <div class="rating header-icon">
                   <img src="/matcha/webroot/images/star.png"><i><?php echo $data[0]['user_rating']?></i>
               </div>
               <div class="menu">
                   <ul>
                       <li><a href="/matcha/encounters">Encounters</a></li><!--
                       --><li><a href="/matcha/search">People</a></li><!--
                       --><li><a href="/matcha/messages">Messages</a></li>
                   </ul>
               </div>
           </div>
       </div>
       <div class="main">
           <div class="center">
               <?php include 'view/' . $content_view; ?>
           </div>
       </div>
       <div class="footer">&copy 2018 ksarnyts/dbessmer. All rights reserved</div>
   </div>
    <input type="hidden" id="user-id" value="<?php echo $data[0]['user_id']?>">
</body>
</html>

<script type="text/javascript" src="/matcha/webroot/js/menu.js"></script>
<script type="text/javascript" src="/matcha/webroot/js/ajax.js"></script>