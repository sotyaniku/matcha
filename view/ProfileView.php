<div class="profile">
    <form class="edit-profile" onsubmit="return sendForm(this);">
        <div>
            <input type="hidden" name="user_id" value="<?php echo $data[0]['user_id'];?>">
            <span>Username</span><br>
            <input name="username" value="<?php echo $data[0]['user_login'];?>" readonly><br>
            <span>Gender</span><br>
            <select name="gender">
                <option value="man" <?php if ($data[0]['user_gender'] == "man") echo "selected";?>>Man</option>
                <option value="woman" <?php if ($data[0]['user_gender'] == "woman") echo "selected";?>>Woman</option>
            </select><br>
            <span>Sexual preferences</span><br>
            <select name="preferences">
                <option value="heterosexual"
                    <?php if ($data[0]['user_preferences'] == "heterosexual") echo "selected";?>>Heterosexual</option>
                <option value="homosexual"
                    <?php if ($data[0]['user_preferences'] == "homosexual") echo "selected";?>>Homosexual</option>
                <option value="bisexual"
                    <?php if ($data[0]['user_preferences'] == "bisexual") echo "selected";?>>Bisexual</option>
            </select><br>
            <span>Firstname</span><br>
            <input name="firstname" value="<?php echo $data[0]['user_firstname'];?>"><br>
            <span>Lastname</span><br>
            <input name="lastname" value="<?php echo $data[0]['user_lastname'];?>"><br>
            <span>Age</span><br>
            <input name="age" value="<?php echo $data[0]['user_age'] ? $data[0]['user_age'] : "";?>"><br>
            <span>City</span><br>
            <input name="city" id="city" readonly>
            <img src="/matcha/webroot/images/map.png" width="24px" onclick="showMap()"><br>
            <input type="hidden" id="country" name="country">
            <div id="container">
                <div id="map"></div>
                <img id="close-btn" src="/matcha/webroot/images/close.png" width="24px" onclick="hideMap()">
            </div>
            <div id="white-container">
                <?php foreach ($additional_data as $interest) :?>
                    <input type="checkbox" name="interest[]"
                           value="<?php echo $interest['interest_id']?>"
                           id="<?php echo $interest['interest_id']?>"
                        <?php if ($interest['user_login']) echo "checked";?>><!--
                    --><label for="<?php echo $interest['interest_id']?>"><?php echo $interest['interest_name'];?></label>
                <?php endforeach;?>
                <img id="close-list-btn" src="/matcha/webroot/images/close.png" width="24px" onclick="hideInterests()">
            </div>
            <input type="hidden" name="latitude" id="latitude"
                   value="<?php echo $data[0]['user_latitude'];?>">
            <input type="hidden" name="longitude" id="longitude"
                   value="<?php echo $data[0]['user_longitude'];?>">
            <span>Email</span><br>
            <input name="email" value="<?php echo $data[0]['user_mail'];?>"><br>
            <span>Describe yourself</span><br>
            <textarea name="biography"><?php echo $data[0]['user_biography']? $data[0]['user_biography'] : "";?></textarea>
        </div>
        <div>
            <span>Your profile photo</span>
            <div class="photo">
                <div class="img" <?php echo $data[0]['user_profilephoto'] ?
                    "style='background-image: url(" . $data[0]['user_profilephoto'] . "');" : "";?>></div>
                <label for="photo" class="button">Choose the file</label>
                <input id="photo" type="file" accept="image/*" onchange="uploadPhoto(event);">
                <input id="photo-src" type="hidden" name="photo-src">
            </div>
            <span>New password</span><br>
            <input name="password" type="password"><br>
            <span>New password (confirm)</span><br>
            <input name="password_2" type="password"><br>
            <span>Your interests</span>
            <div class="interest">
                <ul>
                    <?php foreach ($data as $interest) :?><!--
                        --><li><?php echo $interest['interest_name'];?></li><!--
                    --><?php endforeach;?>
                </ul>
                <label class="button" onclick="showInterests();">browse...</label>
            </div>
        </div>
        <div>
            <p class="error_msg"><?php ?></p>
            <input type="submit" name="update-profile" value="Update information"><!--
            --><span class="checkmark"></span>
        </div>
    </form>
</div>

<script type="text/javascript" src="/matcha/webroot/js/profile.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCCY-CKrzmHIXhG5-y45vWI4TD0g05vICw&callback=initialize"></script>
<script type="text/javascript" src="/matcha/webroot/js/ajax.js"></script>
<script type="text/javascript" src="/matcha/webroot/js/updateProfile.js"></script>

