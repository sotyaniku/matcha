<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Matcha</title>
    <link rel="stylesheet" type="text/css" href="/matcha/webroot/css/mainstyle.css">
    <link rel="stylesheet" type="text/css" href="/matcha/webroot/css/mobile.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Indie+Flower">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

</head>
<body>
   <div>
       <div class="header">
           <div class="center">
               <div class="logo">
                   <a href="/matcha/"><h1>Matcha</h1></a>
               </div>
           </div>
       </div>
       <div class="main">
           <div class="center">
               <?php include 'view/' . $content_view; ?>
           </div>
       </div>
       <div class="footer">&copy dbessmer
           2017</div>
   </div>
</body>
</html>