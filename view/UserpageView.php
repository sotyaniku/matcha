<?php
$photo = (!empty($additional_data[0][0]['user_profilephoto'])) ? $additional_data[0][0]['user_profilephoto'] : "/matcha/webroot/images/avatar.png";
$ownPage = $additional_data[0][0]['user_id'] === $data[0]['user_id'] ? 1 : 0;
$page_is_liked = $additional_data[2];
$friends = $additional_data[2] && $additional_data[3] ? 1 : 0;
?>

<div class="userpage">
    <div class="top-right">
        <?php switch ($ownPage) {
            case 0:
                if (empty($data[0]['user_profilephoto']))
                    $content = '';
                else
                    $content = <<<HTML
            <form method="post">
            <input type="hidden" name="like" value="{$page_is_liked}">
            <input type="image" src="/matcha/webroot/images/like.png">
            <img class="checkmark-{$page_is_liked}" src="/matcha/webroot/images/checkmark.png">
            </form>
HTML;
                break;
            case 1:
                $content = <<<HTML
            <a class="button" href="profile">Profile info</a>
HTML;
                break;
        }
        echo $content;
        ?>
    </div>
    <div class="user-main-info">
        <i style="background-image: url(<?php echo $photo; ?>);">
            <a href="/matcha/messages/<?php echo $additional_data[0][0]['user_login'] ?>">
                <img src="/matcha/webroot/images/love_letter.png" class="friends-<?php echo $friends; ?>">
            </a>
        </i>
        <b id="status"></b>
        <h3><?php echo $additional_data[0][0]['user_login'] ?><span id="last-time-online"></span></h3>
    </div>
    <div class="self header-icon has-list">
        <?php if (!$ownPage) {
            echo "<div class='dots'>
            <span class='dot'></span>
            <span class='dot'></span>
            <span class='dot'></span>
            </div>";
        } ?>
        <form method="post">
            <ul>
                <li><input type="submit" name="block" value="Block user"></li>
                <li><input type="submit" name="fake" value="Report fake account"></li>
            </ul>
        </form>
        <b class="arrow-border"></b>
        <b class="arrow"></b>
    </div>

    <div class="user-photos">
        <div class="gallery">
            <?php foreach ($additional_data[0] as $user) {
                if (!empty($user['photo_src'])) {
                    switch ($ownPage) {
                        case 0:
                            $photo = <<<HTML
                <div style="background-image: url({$user['photo_src']});"></div>
HTML;
                            break;
                        case 1:
                            $photo = <<<HTML
                <div style="background-image: url({$user['photo_src']});">
                    <form class="delete-photo" method="post">
                        <input type="hidden" name="delete-photo-id" value="{$user['photo_id']}">
                        <input type="image" name="delete-image" src="/matcha/webroot/images/close.png">
                    </form>
                </div>
HTML;
                            break;
                    }
                    echo $photo;
                }
            } ?>
            <?php
            if ($ownPage && empty($additional_data[0][3]['user_id'])) {
                $form = <<<HTML
            <form class="upload-photo" method="post">
                <div class="user-photo"></div>
                <label for="photo" class="button"><b>+</b> Add photo</label>
                <input id="photo" type="file" accept="image/*" onchange="uploadPhoto(event);">
                <input id="photo-src" type="hidden" name="photo-src">
                <input type="image" name="save-image" id="save-icon" src="/matcha/webroot/images/save.png">
            </form>
HTML;
                echo $form;
            }
            ?>
        </div>
    </div>
    <div class="user-other-info">
        <div>
            <h3>Name</h3>
            <p><?php echo $additional_data[0][0]['user_firstname'] ?></p>
            <h3>Lastname</h3>
            <p><?php echo $additional_data[0][0]['user_lastname'] ?></p>
            <h3>Location</h3>
            <p><?php echo $additional_data[0][0]['user_country'] ?></p>
            <h3>Age</h3>
            <p><?php echo $additional_data[0][0]['user_age'] ?></p>
            <h3>Description</h3>
            <p><?php echo $additional_data[0][0]['user_biography'] ?></p>
        </div>
        <div>
            <i><?php echo $additional_data[0][0]['user_rating'] ?></i>
            <h3>Gender</h3>
            <p><?php echo $additional_data[0][0]['user_gender'] ?></p>
            <h3>Preferences</h3>
            <p><?php echo $additional_data[0][0]['user_preferences'] ?></p>
            <h3>Interests</h3>
            <?php
            if (!empty($additional_data[1])) {
                echo "<ul>";
                foreach ($additional_data[1] as $user)
                    echo "<li>" . $user['interest_name'] . "</li>";
                echo "</ul>";
            } else if (!empty($additional_data[0][0]['interest_name'])) {
                echo "<ul>";
                foreach ($additional_data[0] as $user)
                    echo "<li>" . $user['interest_name'] . "</li>";
                echo "</ul>";
            }
            ?>
        </div>
    </div>
</div>


<script type="text/javascript" src="/matcha/webroot/js/userpage.js"></script>
<script type="text/javascript" src="/matcha/webroot/js/ajax.js"></script>

