<div class="list">
    <?php if (!empty($additional_data[0])) {
        $content = "<h3>Here's the list of your visitors</h3>";
        $content .= "<ul>";
        foreach ($additional_data as $visit) {
            $content .= "<li>";
            $content .= "<a href='" . $visit['user_login'] . "'>";
            $content .= "<span>" . $visit['visit_time'] . "</span>";
            $content .= $visit['user_login'];
            $photo = $visit['user_profilephoto'] ? $visit['user_profilephoto'] : '/matcha/webroot/images/avatar.png';
            $content .= "<i style='background-image: url(" . $photo  . ")'></i>";
            $content .= "</a>";
            $content .= "</li>";
        }
        $content .= "</ul>";
    }
    else {
        $content = "<h3>No one has visited your page</h3>";
    }
    echo $content;
    ?>
</div>
