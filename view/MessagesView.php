<div class="messages">
        <?php if (empty($additional_data['messages']) && empty($additional_data['companion'])) {
            $content = "<div class='list'><h3>You have no messages</h3></div>";

        } else {
            $content = "<div class='list chat' style='width: 25%;margin: 0; display: inline-block'><h3>Messages</h3>
                        <ul style='max-height: 400px'>";
            foreach ($additional_data['messages'] as $message) {
                $content .= "<li class='chat-item'>";
                $content .= "<a href='" . SITE_BASE_URI . "messages/" . $message['user_login'] . "'>";
                $content .= "<span class='chat-name'>" . $message['user_login'] . "</span>";
                $photo = $message['user_profilephoto'] ? $message['user_profilephoto'] : '/matcha/webroot/images/avatar.png';
                $content .= "<i style='background-image: url(" . $photo . ")'></i>";
                $content .= "</a>";
                $content .= "</li>";
            }
            $content .= "</ul>";
            $content .= "</div>";

            if (!empty($additional_data['companion'])) {
                $content .= "<div class='message'><ul>";

                foreach ($additional_data['chat'] as $chatMessage) {
                    $content .= "<li>";

                    $content .= "<div>";
                    $photo = $chatMessage['user_profilephoto'] ? $chatMessage['user_profilephoto'] : '/matcha/webroot/images/avatar.png';

                    $content .= "<i style='background-image: url(" . $photo . ")'></i>";
                    $content .= "<br><span class='login'>" . $chatMessage['user_login'] . "</span>";
                    $content .= "</div>";
                    $content .= "<span class='message-item'>";
                    $content .= "<span class='time' style='font-size: 8px;line-height: 1em;'>" . date('d.m.y h:i', strtotime($chatMessage['message_time'])) . "</span>";
                    $content .= "<span class='text' style='line-height: 3em;'>" . $chatMessage['message_text'] . "</span>";
                    $content .= "<span>";

                    $content .= "</li>";

                }
                $content .= "</ul>";
                if (!$additional_data['blocked']) {
                    $content .= "<form action='' method='post'>";
                    $content .= "<textarea autofocus name='message'></textarea>";
                    $content .= "<button type='submit' name='sendMessage'><img src='/matcha/webroot/images/plane.png'></button>";
                    $content .= "</form>";
                }
            }
            $content .= "</div>";

        }


        echo $content;
        ?>
</div>

<script type="text/javascript" src="/matcha/webroot/js/message.js"></script>
<script type="text/javascript" src="/matcha/webroot/js/ajax.js"></script>
