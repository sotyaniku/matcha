<div class="list">
    <?php if (!empty($additional_data[0])) {
        $content = "<h3>Unread notifications</h3>";
        $content .= "<ul>";
        foreach ($additional_data as $visit) {
            $status = $visit['type'] === 'like' ? ( $visit['like_status'] == 1 ? '?like=1' : '?unlike=1') : '?visit=1';

            $content .= "<li>";
            $content .= "<a href='" . $visit['user_login'] . $status . "'>";
            $content .= "<span>" . $visit['visit_time'] . "</span>";
            $content .= $visit['user_login'];
            if ($visit['type'] === 'like')
                $content .= $visit['like_status'] == 1 ? '<span> liked your account</span>' :
                    '<span> unliked your account</span>';
            else

            $content .= '<span> visited your page</span>';
            $photo = $visit['user_profilephoto'] ? $visit['user_profilephoto'] : '/matcha/webroot/images/avatar.png';
            $content .= "<i style='background-image: url(" . $photo  . ")'></i>";
            $content .= "</a>";
            $content .= "</li>";
        }
        $content .= "</ul>";
    }
    else {
        $content = "<h3>No new notifications</h3>";
    }
    echo $content;
    ?>
</div>
