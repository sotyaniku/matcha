<?php
$rating = isset($_POST['rating']) ? $_POST['rating'] : '';
$distance = isset($_POST['distance']) ? $_POST['distance'] : '';
$tags = isset($_POST['tags']) ? $_POST['tags'] : '';

?>

<div class="sort">
    <div>
        <h5>order</h5><!--
        --><a href="/matcha/encounters/order-rating/">rating</a><!--
        --><a href="/matcha/encounters/order-distance/">distance</a><!--
        --><a href="/matcha/encounters/order-tags/">tags</a>
    </div>
    <div>
        <form method="post" action="/matcha/encounters">
        <h5>sort</h5><!--
        --><select name="rating">
            <option disabled selected>rating</option>
            <option value="3" <?php echo $rating === '3' ? "selected" : ''?>>3 and more</option>
            <option value="4" <?php echo $rating === '4' ? "selected" : ''?>>4 and more</option>
            <option value="5" <?php echo $rating === '5' ? "selected" : ''?>>only 5</option>
            <option value="0">any rating</option>
        </select><!--
        --><select name="distance">
            <option disabled selected>distance</option>
            <option value="2" <?php echo $distance === '2' ? "selected" : ''?>>20 km</option>
            <option value="5" <?php echo $distance === '5' ? "selected" : ''?>>50 km</option>
            <option value="10" <?php echo $distance === '10' ? "selected" : ''?>>100 km</option>
            <option value="100">any distance</option>
        </select><!--
        --><select name="tags">
            <option disabled selected>tags</option>
            <option value="1" <?php echo $tags === '1' ? "selected" : ''?>>1 or 2 common tags</option>
            <option value="3" <?php echo $tags === '3' ? "selected" : ''?>>3 to 5 common tags</option>
            <option value="5" <?php echo $tags === '5' ? "selected" : ''?>>more than 5 common tags</option>
            <option value ="0">any number of common tags</option>
        </select><!--
            --><input type="submit" name="sort" value="filter data">
        </form>
    </div>
</div>

<div class="list">
    <?php if (!empty($additional_data[0])) {
        $content = "<h3>People you may like</h3>";
        $content .= "<ul>";
        foreach ($additional_data as $user) {
            $content .= "<li>";
            $content .= "<a href='" . SITE_BASE_URI . $user['user_login'] . "'>";
            $content .= "<span>" . $user['distance'] * 10 . " km from you</span>";
            $content .= $user['user_login'] . "<p>(" . $user['common_interests'] . " common tags)</p>";
            $photo = $user['user_profilephoto'] ? $user['user_profilephoto'] : '/matcha/webroot/images/avatar.png';
            $content .= "<i style='background-image: url(" . $photo  . ")'></i>";
            $content .= "<b>" . $user['user_rating'] . "</b>";
            $content .= "</a>";
            $content .= "</li>";
        }
        $content .= "</ul>";
    }
    else {
        $content = "<h3>There is no one who matches you</h3>";
    }
    echo $content;
    ?>
</div>

