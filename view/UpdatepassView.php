<div class="register">
    <div class="updatepass">
        <h3>Update your password</h3>
        <form class="update-pass" action="/matcha/updatepass/" method="post" onsubmit="return validateForm(this);">
            <span>New password</span><br>
            <input name="password" type="password"><br>
            <span>New password (confirm)</span><br>
            <input name="password_2" type="password"><br>
            <input name="id" type="hidden" value="<?php echo "{$data['id']}";?>">
            <p class="error_msg"></p>
            <input type="submit" name="updatepass" value="Update password">
        </form>
    </div>
</div>

<script type="text/javascript" src="/matcha/webroot/js/registration.js"></script>