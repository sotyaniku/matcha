<div class="register">
    <div>
        <input type="radio" name="reg-tab" id="r1" checked>
        <label for="r1">Register</label>
        <form class="reg" onsubmit="return sendForm(this);">
            <span>Gender</span><br>
            <select name="gender">
                <option value="man" selected>Man</option>
                <option value="woman">Woman</option>
            </select><br>
            <span>Sexual preferences</span><br>
            <select name="preferences">
                <option value="heterosexual">Heterosexual</option>
                <option value="homosexual">Homosexual</option>
                <option value="bisexual" selected>Bisexual</option>
            </select><br>
            <span>Username</span><br>
            <input name="username"><br>
            <span>Email</span><br>
            <input name="email"><br>
            <span>Firstname</span><br>
            <input name="firstname"><br>
            <span>Lastname</span><br>
            <input name="lastname"><br>
            <span>Password</span><br>
            <input name="password" type="password">
            <span>Password (confirm)</span><br>
            <input name="password_2" type="password">
            <p class="error_msg"></p>
            <input type="hidden" name="latitude" id="latitude">
            <input type="hidden" name="longitude" id="longitude">
            <input type="hidden" name="country" id="country">
            <input type="submit" name="register" value="Register">
        </form>
        <input type="radio" name="reg-tab" id="r2">
        <label for="r2">Log in</label>
        <form class="log-in" onsubmit="return sendForm(this);">
             <span>Username</span><br>
            <input name="username"><br>
            <span>Password</span><br>
            <input name="password" type="password">
            <p class="error_msg">Forgot your password? Click <a href="sendpass">here</a> for update</p>
            <input type="submit" name="log-in" value="Log in">
        </form>
    </div>
</div>

<script type="text/javascript" src="/matcha/webroot/js/ajax.js"></script>
<script type="text/javascript" src="/matcha/webroot/js/registration.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCCY-CKrzmHIXhG5-y45vWI4TD0g05vICw"></script>