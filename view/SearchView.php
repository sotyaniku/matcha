<?php
$rating = isset($_POST['rating']) ? $_POST['rating'] : '';
$distance = isset($_POST['distance']) ? $_POST['distance'] : '';
$tags = isset($_POST['tags']) ? $_POST['tags'] : '';

?>



<form id="form1" method="post" action="/matcha/search">
<div class="search">
    <div>
            <h5>Rating gap</h5>
            <input type="number" step="0.1" max="5" min="0" name="rating-from" value="<?php echo $data['search']['rating_from'];?>">
            <input type="number" step="0.1" max="5" min="0" name="rating-to" value="<?php echo $data['search']['rating_to'];?>"><br>
            <h5>Age gap</h5>
            <input type="number" step="1" max="100" name="age-from" value="<?php echo $data['search']['age_from'];?>">
            <input type="number" step="1" max="100" min="18" name="age-to" value="<?php echo $data['search']['age_to'];?>"><br>
            <h5>Distance gap</h5>
            <input type="number" step="any" max="25000" min="0" name="distance-from" value="<?php echo $data['search']['distance_from'];?>">
            <input type="number" step="any" max="25000" min="0" name="distance-to" value="<?php echo $data['search']['distance_to'];?>"><br>
            <span>
            <h5>Interests</h5>
            <select name="interest[]" multiple="multiple" size="5">
                <option disabled>Select 1 or more interests</option>
                <option value="any">any interest</option>
                <?php foreach ($data['int_list'] as $interest): ?>
                <option value="<?php echo $interest['interest_name'];?>"
                    <?php if (isset($_POST['interest']) && in_array($interest['interest_name'], $_POST['interest']))
                        echo " selected";?>>
                    <?php echo $interest['interest_name']?>
                </option>
                <?php endforeach; ?>
            </select>
            <input type="submit" name="search" value="filter data" form="form1">
            </span>
    </div>
</div>

<div class="list">
    <?php if (!empty($additional_data[0])) {
        $content = "<h3>People you may like</h3>";
        $content .= "<ul>";
        foreach ($additional_data as $user) {
            if ($user['common_interests'] > 0 || !isset($_POST['interest']) || $_POST['interest'][0] == 'any') {

                $content .= "<li>";
                $content .= "<a href='" . SITE_BASE_URI . $user['user_login'] . "'>";
                $content .= "<span>" . $user['distance'] . " km from you</span>";
                $content .= $user['user_login'] . "<p>(" . $user['common_interests'] . " tags)</p>";
                $photo = $user['user_profilephoto'] ? $user['user_profilephoto'] : '/matcha/webroot/images/avatar.png';
                $content .= "<i style='background-image: url(" . $photo  . ")'></i>";
                $content .= "<b>" . $user['user_rating'] . "</b>";
                $content .= "</a>";
                $content .= "</li>";
            }
        }
        $content .= "</ul>";
    }
    else {
        $content = "<h3>There is no one who matches the request</h3>";
    }
    echo $content;
    ?>
</div>

<div class="sort" <?php if (empty($additional_data[0])) echo "style='display: none;'"?>>
    <div>
        <h5>order</h5>
        <input type="radio" value="order-rating" name="order" id="rating" style="display:none">
        <label for="rating">rating</label>
        <input type="radio" value="order-distance" name="order" id="distance" style="display:none">
        <label for="distance">distance</label>
        <input type="radio" value="order-tags" name="order" id="tags" style="display:none">
        <label for="tags">tags</label>
    </div>
    <div>
        <h5>sort</h5><!--
        --><select name="rating">
            <option disabled selected>rating</option>
            <option value="3" <?php echo $rating === '3' ? "selected" : ''?>>3 and more</option>
            <option value="4" <?php echo $rating === '4' ? "selected" : ''?>>4 and more</option>
            <option value="5" <?php echo $rating === '5' ? "selected" : ''?>>only 5</option>
            <option value="0">any rating</option>
        </select><!--
        --><select name="distance">
            <option disabled selected>distance</option>
            <option value="2" <?php echo $distance === '2' ? "selected" : ''?>>20 km</option>
            <option value="5" <?php echo $distance === '5' ? "selected" : ''?>>50 km</option>
            <option value="10" <?php echo $distance === '10' ? "selected" : ''?>>100 km</option>
            <option value="100">any distance</option>
        </select><!--
        --><select name="tags">
            <option disabled selected>tags</option>
            <option value="1" <?php echo $tags === '1' ? "selected" : ''?>>1 or 2 common tags</option>
            <option value="3" <?php echo $tags === '3' ? "selected" : ''?>>3 to 5 common tags</option>
            <option value="5" <?php echo $tags === '5' ? "selected" : ''?>>more than 5 common tags</option>
            <option value ="0">any number of common tags</option>
        </select><!--
            --><input type="submit" name="sort" value="filter data" form="form1">
    </div>
</div>
</form>