-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2018 at 02:45 AM
-- Server version: 5.7.20
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matcha`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blocked`
--

CREATE TABLE `tbl_blocked` (
  `block_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blocked`
--

INSERT INTO `tbl_blocked` (`block_id`, `user_id`, `profile_id`) VALUES
(15, 11, 8),
(29, 12, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hashtag`
--

CREATE TABLE `tbl_hashtag` (
  `hashtag_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `interest_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_hashtag`
--

INSERT INTO `tbl_hashtag` (`hashtag_id`, `user_id`, `interest_id`) VALUES
(98, 7, 23),
(100, 7, 29),
(102, 7, 19),
(103, 7, 11),
(105, 2, 13),
(115, 2, 12),
(134, 2, 36),
(135, 2, 37),
(138, 7, 3),
(139, 10, 36),
(142, 2, 11),
(143, 10, 37),
(144, 11, 1),
(145, 11, 2),
(146, 11, 10),
(147, 11, 11),
(148, 11, 13),
(149, 11, 15),
(150, 11, 26),
(151, 11, 27),
(152, 11, 29),
(153, 11, 33),
(154, 11, 34),
(155, 11, 38);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_interests`
--

CREATE TABLE `tbl_interests` (
  `interest_id` int(11) NOT NULL,
  `interest_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_interests`
--

INSERT INTO `tbl_interests` (`interest_id`, `interest_name`) VALUES
(1, 'traveling'),
(2, 'exercise'),
(3, 'theatre'),
(4, 'dancing'),
(5, 'cooking'),
(6, 'doing stuff outdoors'),
(7, 'politics'),
(8, 'pets'),
(9, 'photography'),
(10, 'sports'),
(11, 'art'),
(12, 'learning'),
(13, 'swimming'),
(15, 'music'),
(16, 'adrenaline'),
(19, 'movies'),
(20, 'friends'),
(21, 'family'),
(22, 'beach'),
(23, 'reading'),
(24, 'concerts'),
(25, 'tattoo'),
(26, 'shopping'),
(27, 'wine'),
(28, 'fishing'),
(29, 'camping'),
(30, 'golf'),
(31, 'football'),
(32, 'chess'),
(33, 'surfing'),
(34, 'hiking'),
(35, 'horses'),
(36, 'cats'),
(37, 'dogs'),
(38, 'salsa'),
(39, 'watching TV');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_likes`
--

CREATE TABLE `tbl_likes` (
  `like_id` int(11) NOT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `like_seen` int(11) DEFAULT '0',
  `like_status` int(11) DEFAULT '1',
  `like_time` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT 'like'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_likes`
--

INSERT INTO `tbl_likes` (`like_id`, `visitor_id`, `profile_id`, `like_seen`, `like_status`, `like_time`, `type`) VALUES
(15, 7, 2, 1, 1, '2018-02-12 02:44:16', 'like'),
(19, 2, 7, 1, 1, '2018-02-12 02:43:03', 'like'),
(20, 11, 8, 0, 1, '2018-07-26 07:58:54', 'like'),
(21, 12, 11, 0, 1, '2018-08-05 00:17:48', 'like'),
(22, 11, 12, 1, 1, '2018-08-02 12:33:26', 'like'),
(23, 12, 8, 0, 1, '2018-08-01 08:31:03', 'like'),
(24, 12, 2, 0, 1, '2018-08-05 00:18:23', 'like'),
(25, 2, 12, 1, 1, '2018-08-04 09:46:47', 'like');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lover_id` int(11) NOT NULL,
  `message_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `user_id`, `lover_id`, `message_time`, `message_text`) VALUES
(1, 11, 12, '2018-07-27 22:42:19', 'hi, how r u?'),
(2, 10, 12, '2018-07-27 22:42:56', 'wow?'),
(3, 11, 8, '2018-07-27 22:43:18', 'I know)))'),
(4, 12, 11, '2018-07-27 22:44:12', 'Im good, tnx, and u'),
(5, 11, 12, '2018-07-27 22:44:28', 'wanna hang out?'),
(9, 11, 12, '2018-08-01 18:19:04', 'sure, how about Saturday?'),
(10, 12, 11, '2018-08-01 18:20:49', 'u answered your own question)))'),
(11, 12, 11, '2018-08-01 18:20:54', 'u answered your own question)))'),
(12, 11, 12, '2018-08-01 18:21:06', 'sure, how about Saturday?'),
(19, 12, 11, '2018-08-01 18:28:32', 'u answered your own question)))'),
(20, 12, 11, '2018-08-01 18:31:30', 'ksndpshdpouHD;FASDBFHADLSFJB\'NkwdfnlDFN\r\nLHBDFI;JWef;djbkdF\r\nLHSDBLIbhdf'),
(21, 12, 11, '2018-08-01 18:31:31', ''),
(22, 12, 11, '2018-08-01 18:32:03', 'smdklfn;sd<br />\r\nsdflknsdlf<br />\r\nnnee<br />\r\n2222'),
(27, 11, 12, '2018-08-02 10:47:40', '1111111111111'),
(28, 12, 11, '2018-08-02 10:47:57', 'werwerwerwer'),
(29, 11, 12, '2018-08-03 22:42:49', 'rterterterterter'),
(30, 12, 11, '2018-08-04 18:50:25', 'bsdfblsdf'),
(31, 2, 12, '2018-08-04 19:52:25', 'fnlsdfnlksndf'),
(32, 12, 11, '2018-08-04 20:44:02', 'qqqqq'),
(34, 12, 2, '2018-08-04 20:51:36', 'efdfdasf'),
(35, 12, 10, '2018-08-05 09:53:36', 'yeah, I know)<br />\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_photos`
--

CREATE TABLE `tbl_photos` (
  `photo_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `photo_src` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_photos`
--

INSERT INTO `tbl_photos` (`photo_id`, `user_id`, `photo_src`) VALUES
(1, 2, '/matcha/webroot/photos/2/5a3cfcadd9104.jpeg'),
(9, 7, '/matcha/webroot/photos/7/5a701f248e3df.jpeg'),
(10, 7, '/matcha/webroot/photos/7/5a701f34ddd46.jpeg'),
(17, 2, '/matcha/webroot/photos/2/5a702ff44bf3b.jpeg'),
(18, 2, '/matcha/webroot/photos/2/5a7030016ab2b.jpeg'),
(19, 2, '/matcha/webroot/photos/2/5a70300eb00d7.jpeg'),
(20, 11, '/matcha/webroot/photos/11/5b598dabd38cc.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sessions`
--

CREATE TABLE `tbl_sessions` (
  `key_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `last_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sessions`
--

INSERT INTO `tbl_sessions` (`key_id`, `user_id`, `session_id`, `last_time`) VALUES
(27, 7, 'c45mu63muodj50p21rrlkr3m5m', '2018-02-14 12:50:41'),
(28, 2, '3n1qe1hrahnk5ho34r11359m7q', '2018-08-04 19:52:25'),
(29, 8, 'c45mu63muodj50p21rrlkr3m5m', '2018-02-14 14:06:29'),
(30, 10, 'jmpvjm5or6d0qbt334p0q4bl1r', '2018-02-14 09:19:05'),
(31, 11, '3n1qe1hrahnk5ho34r11359m7q', '2018-08-05 10:18:51'),
(32, 12, 'melr6q7e2p7oo42ajk5t900up5', '2018-08-05 12:35:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_login` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_mail` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_gender` varchar(255) NOT NULL,
  `user_preferences` varchar(255) NOT NULL,
  `user_biography` varchar(255) DEFAULT NULL,
  `user_rating` float DEFAULT '4.5',
  `user_age` int(11) DEFAULT NULL,
  `user_latitude` float DEFAULT '0',
  `user_longitude` float NOT NULL DEFAULT '0',
  `user_profilephoto` varchar(255) DEFAULT NULL,
  `user_country` varchar(255) DEFAULT NULL,
  `user_fake` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_login`, `user_firstname`, `user_lastname`, `user_mail`, `user_password`, `user_gender`, `user_preferences`, `user_biography`, `user_rating`, `user_age`, `user_latitude`, `user_longitude`, `user_profilephoto`, `user_country`, `user_fake`) VALUES
(2, 'katya', 'qwe', 'qw', 'qwe@qw', '3fe5418628e525e4d8bd163ac688403cb513bd2af5f734c661652ac40ff2c3eb23f110a5c0c5106001fa487c40615eacc0625b807fe4cde97ae4daf1927a3863', 'woman', 'heterosexual', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mollis tincidunt porttitor. Quisque mollis mauris sollicitudin ante elementum dignissim. Sed eros ante, convallis vel magna cursus, faucibus tristique nisl. Pellentesque habitant morbi volutpat', 1.6, 19, 50.455, 30.3002, '/matcha/webroot/photos/2/5a3cfcadd9104.jpeg', 'UA', 1),
(7, 'sotyaniku', 'Katya', 'Sarnytska', 'sotyaniku@gmail.com', '3fe5418628e525e4d8bd163ac688403cb513bd2af5f734c661652ac40ff2c3eb23f110a5c0c5106001fa487c40615eacc0625b807fe4cde97ae4daf1927a3863', 'woman', 'bisexual', 'cxa', 4, 30, 50.0608, 29.9062, '/matcha/webroot/photos/7/5a5e0fbfc0e97.jpeg', 'UA', 0),
(8, 'qqq', 'qq', 'qq', 'qq@qq.com', '3fe5418628e525e4d8bd163ac688403cb513bd2af5f734c661652ac40ff2c3eb23f110a5c0c5106001fa487c40615eacc0625b807fe4cde97ae4daf1927a3863', 'man', 'bisexual', '', 4.5, 20, 50.4613, 30.2846, '', 'UA', 0),
(10, 'test', 'test', 'test', 'test@ee', '3fe5418628e525e4d8bd163ac688403cb513bd2af5f734c661652ac40ff2c3eb23f110a5c0c5106001fa487c40615eacc0625b807fe4cde97ae4daf1927a3863', 'man', 'bisexual', NULL, 3, 0, 50.4642, 30.4665, NULL, 'UA', 0),
(11, 'Princess', 'Mrs', 'Yo', 'dashko88@gmail.com', '2defbd8a6ddda3bc62c90fdb00e8d0cdac93d867a617ab5ce0ff92f3d31356b3fa9553e25169aa1b3d0da8f350cd9cfe113e089571755919653a5b07ceac6f9c', 'woman', 'heterosexual', NULL, 2.8, NULL, 50.5145, 30.454, '/matcha/webroot/photos/11/5b5e2da58e836.jpeg', 'UA', 1),
(12, 'Prince', 'Mr', 'Yo', 'fff@df.er', '2defbd8a6ddda3bc62c90fdb00e8d0cdac93d867a617ab5ce0ff92f3d31356b3fa9553e25169aa1b3d0da8f350cd9cfe113e089571755919653a5b07ceac6f9c', 'man', 'bisexual', NULL, 3, NULL, 50.5144, 30.4541, '/matcha/webroot/photos/12/5b59eae4a684a.jpeg', 'UA', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visits`
--

CREATE TABLE `tbl_visits` (
  `visit_id` int(11) NOT NULL,
  `visitor_id` int(11) DEFAULT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `visit_seen` int(11) DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT 'visit'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_visits`
--

INSERT INTO `tbl_visits` (`visit_id`, `visitor_id`, `profile_id`, `visit_seen`, `visit_time`, `type`) VALUES
(3, 7, 2, 1, '2018-02-13 01:33:20', 'visit'),
(6, 8, 2, 1, '2018-02-13 23:19:24', 'visit'),
(8, 2, 8, 0, '2018-02-14 00:50:59', 'visit'),
(9, 2, 7, 1, '2018-02-12 03:02:35', 'visit'),
(10, 10, 7, 1, '2018-02-12 03:09:33', 'visit'),
(11, 7, 10, 1, '2018-02-12 03:10:12', 'visit'),
(12, 2, NULL, 0, '2018-02-14 00:35:04', 'visit'),
(13, 2, NULL, 0, '2018-02-14 01:09:25', 'visit'),
(14, 2, NULL, 0, '2018-02-14 01:09:30', 'visit'),
(15, 2, NULL, 0, '2018-02-14 01:15:57', 'visit'),
(16, NULL, 7, 0, '2018-02-14 02:50:14', 'visit'),
(17, 11, 8, 0, '2018-07-29 13:23:24', 'visit'),
(18, 12, 11, 0, '2018-08-05 02:35:55', 'visit'),
(19, 11, 12, 1, '2018-08-03 13:11:44', 'visit'),
(20, 11, NULL, 0, '2018-07-27 02:32:45', 'visit'),
(21, 12, 2, 0, '2018-08-05 00:18:25', 'visit'),
(22, 12, 8, 0, '2018-08-04 09:36:39', 'visit'),
(23, 12, NULL, 0, '2018-08-02 06:45:21', 'visit'),
(24, 11, NULL, 0, '2018-08-02 06:50:23', 'visit'),
(25, 11, NULL, 0, '2018-08-02 06:50:28', 'visit'),
(26, 11, NULL, 0, '2018-08-02 06:50:28', 'visit'),
(27, 11, NULL, 0, '2018-08-02 06:53:22', 'visit'),
(28, 11, NULL, 0, '2018-08-02 06:53:24', 'visit'),
(29, 11, NULL, 0, '2018-08-02 06:53:45', 'visit'),
(30, 11, NULL, 0, '2018-08-02 06:54:01', 'visit'),
(31, 11, NULL, 0, '2018-08-02 06:54:04', 'visit'),
(32, 11, NULL, 0, '2018-08-02 06:54:08', 'visit'),
(33, 11, NULL, 0, '2018-08-02 06:55:20', 'visit'),
(34, 11, NULL, 0, '2018-08-02 06:56:36', 'visit'),
(35, 11, NULL, 0, '2018-08-02 06:57:42', 'visit'),
(36, 11, NULL, 0, '2018-08-02 06:59:07', 'visit'),
(37, 11, NULL, 0, '2018-08-02 07:03:30', 'visit'),
(38, 11, NULL, 0, '2018-08-02 07:09:02', 'visit'),
(39, 11, NULL, 0, '2018-08-02 07:13:08', 'visit'),
(40, 11, NULL, 0, '2018-08-02 07:13:36', 'visit'),
(41, 11, NULL, 0, '2018-08-02 07:13:41', 'visit'),
(42, 11, NULL, 0, '2018-08-02 07:14:55', 'visit'),
(43, 11, NULL, 0, '2018-08-02 07:20:45', 'visit'),
(44, 11, NULL, 0, '2018-08-02 07:20:54', 'visit'),
(45, 11, NULL, 0, '2018-08-02 07:22:14', 'visit'),
(46, 11, NULL, 0, '2018-08-02 07:25:20', 'visit'),
(47, 11, NULL, 0, '2018-08-02 07:27:22', 'visit'),
(48, 11, 10, 0, '2018-08-03 04:30:02', 'visit'),
(49, 12, 10, 0, '2018-08-03 14:01:27', 'visit'),
(50, 2, 12, 1, '2018-08-04 09:52:05', 'visit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_blocked`
--
ALTER TABLE `tbl_blocked`
  ADD PRIMARY KEY (`block_id`);

--
-- Indexes for table `tbl_hashtag`
--
ALTER TABLE `tbl_hashtag`
  ADD PRIMARY KEY (`hashtag_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `interest_id` (`interest_id`);

--
-- Indexes for table `tbl_interests`
--
ALTER TABLE `tbl_interests`
  ADD PRIMARY KEY (`interest_id`);

--
-- Indexes for table `tbl_likes`
--
ALTER TABLE `tbl_likes`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Indexes for table `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  ADD PRIMARY KEY (`key_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
  ADD PRIMARY KEY (`visit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_blocked`
--
ALTER TABLE `tbl_blocked`
  MODIFY `block_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_hashtag`
--
ALTER TABLE `tbl_hashtag`
  MODIFY `hashtag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `tbl_interests`
--
ALTER TABLE `tbl_interests`
  MODIFY `interest_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbl_likes`
--
ALTER TABLE `tbl_likes`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_photos`
--
ALTER TABLE `tbl_photos`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_sessions`
--
ALTER TABLE `tbl_sessions`
  MODIFY `key_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_visits`
--
ALTER TABLE `tbl_visits`
  MODIFY `visit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_hashtag`
--
ALTER TABLE `tbl_hashtag`
  ADD CONSTRAINT `tbl_hashtag_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`user_id`),
  ADD CONSTRAINT `tbl_hashtag_ibfk_2` FOREIGN KEY (`interest_id`) REFERENCES `tbl_interests` (`interest_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
