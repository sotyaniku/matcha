<?php

const ROUTES = array (
    'confirm' => 'user/confirm',
    'login' => 'user/login',
    'register' => 'user/register',
    'resetpass/([0-9]+)/([a-zA-Z0-9]+)' => 'user/resetpass/$1/$2',
    'resetpass' => 'user/resetpass',
    'sendpass' =>'user/sendpass',
    'updatepass' => 'user/updatepass',
    'updateprofile' => 'user/updateprofile',
    'logout' => 'user/logout',
    'profile' => 'user/profile',
    'history' => 'user/history',
    'likes' => 'user/likes',
    'visitors' => 'user/visitors',
    'status' => 'user/status',
    'notifications' => 'user/shownotifications',
    'notification' => 'user/notification',
    'encounters/([a-z\-]+)' => 'user/encounters/$1',
    'encounters' => 'user/encounters',
    'search/([a-z\-]+)' => 'user/search/$1',
    'search' => 'user/search',
    'messages/([0-9a-zA-Z\/]+)' => 'user/messages/$1',
    'messages' => 'user/messages',
    'matcha/([0-9a-zA-Z\/]+)' => 'matcha/user/userpage/$1',
    'matcha' => 'matcha/user/index'
);
